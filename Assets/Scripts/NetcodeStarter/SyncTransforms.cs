﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Transforms;
using UnityEngine;

[UpdateInGroup(typeof(PresentationSystemGroup))]
public class SyncTransforms : SystemBase {
  private const float SyncFactor = 0.07f;
  protected override void OnUpdate() {
    Entities.WithoutBurst().ForEach(
      (Entity entity, ref SyncTransform sync, in Translation trans, in Rotation rotation) => {
        var obj = EntityManager.GetComponentObject<GameObject>(entity);
        var transform = obj.GetComponent<Transform>();
        sync.positionOffset *= 1f - SyncFactor;
        // sync.rotationOffset.x = sync.rotationOffset.x > 180f
        //   ? sync.rotationOffset.x - 360f
        //   : (sync.rotationOffset.x < -180f ? sync.rotationOffset.x + 360f : sync.rotationOffset.x);
        // sync.rotationOffset.y = sync.rotationOffset.y > 180f
        //   ? sync.rotationOffset.y - 360f
        //   : (sync.rotationOffset.y < -180f ? sync.rotationOffset.y + 360f : sync.rotationOffset.y);
        // sync.rotationOffset.z = sync.rotationOffset.z > 180f
        //   ? sync.rotationOffset.z - 360f
        //   : (sync.rotationOffset.z < -180f ? sync.rotationOffset.z + 360f : sync.rotationOffset.z); 
        // sync.rotationOffset *= 1f - SyncFactor;
        transform.position = trans.Value + sync.positionOffset;
        // transform.rotation = Quaternion.Euler(sync.rotationOffset) * rotation.Value;
        transform.rotation = rotation.Value;
      }).Run();
    // Entities.ForEach((ref EntityProxy proxy,
    //                   ref Translation trans,
    //                   ref Rotation rotation,
    //                   ref PhysicsVelocity velocity) => {
    //   trans.Value = EntityManager.GetComponentData<Translation>(proxy.proxyFor).Value;
    //   rotation.Value = EntityManager.GetComponentData<Rotation>(proxy.proxyFor).Value;
    //   var physicsVelocity = EntityManager.GetComponentData<PhysicsVelocity>(proxy.proxyFor);
    //   velocity.Linear = physicsVelocity.Linear;
    //   velocity.Angular = physicsVelocity.Angular;
    // });
  }
}

[GenerateAuthoringComponent]
public struct SyncTransform : IComponentData {
  public float3 positionOffset;
  // public float3 rotationOffset;
}