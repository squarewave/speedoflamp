﻿using Unity.Collections;
using Unity.Entities;

public struct NetcodePrefabList : IComponentData {
  public Entity prefabs;
  public Entity deathPrefabs;
}

public struct NetcodePrefabBuffer : IBufferElementData {
  public Entity value;
}