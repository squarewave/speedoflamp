﻿#if UNITY_EDITOR
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;
using UnityEngine.Windows;

[CustomEditor(typeof(NetcodePrefabListSingleton))]
public class NetcodePrefabListSingletonEditor : Editor {
  public override void OnInspectorGUI() {
    var prefabList = target as NetcodePrefabListSingleton;
    
    SerializedObject so = new SerializedObject(target);
    SerializedProperty prefabsProperty = so.FindProperty("prefabs");
    SerializedProperty deathPrefabsProperty = so.FindProperty("deathPrefabs");
 
    EditorGUILayout.PropertyField(prefabsProperty, true);
    EditorGUILayout.PropertyField(deathPrefabsProperty, true);
    so.ApplyModifiedProperties();
    
    if (GUILayout.Button("Generate code")) {
      var file = new StringBuilder();
      WriteEnumFromList(file, prefabList.prefabs, "AllNetcodePrefabs");
      file.Append("\n");
      
      File.WriteAllBytes(Application.dataPath + "/Scripts/Generated/AllNetcodePrefabs.cs",
                         Encoding.UTF8.GetBytes(file.ToString()));
    }
    
    if (GUILayout.Button("Refresh")) {
      prefabList.prefabs.Clear();
      prefabList.deathPrefabs.Clear();
      
      var netcodePrefabs = PrefabUtils.GetPrefabsWithComponent<NetcodePrefab>().ToList();
      
      netcodePrefabs.Sort((lhs, rhs) => lhs.sortIndex - rhs.sortIndex);
      foreach (var netcodePrefab in netcodePrefabs) {
        prefabList.prefabs.Add(netcodePrefab.gameObject);
        prefabList.deathPrefabs.Add(netcodePrefab.deathPrefab);
      }
      
      Debug.Log("Refreshing " + prefabList.prefabs.Count);
    }
  }

  private static void WriteEnumFromList(StringBuilder file, List<GameObject> list, string enumName) {
    file.Append("public enum ");
    file.Append(enumName);
    file.Append(" {\n");
    foreach (var obj in list) {
      file.Append("  ");
      file.Append(obj.name);
      file.Append(",\n");
    }

    file.Append("\n  Count\n");
    file.Append("}\n");
  }
}

#endif