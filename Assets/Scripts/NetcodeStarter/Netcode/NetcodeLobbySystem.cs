﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;

public class NetcodeLobbySystem : MonoBehaviour {
  public static NetcodeLobbySystem instance;
  
  public string serverAddress;
  public string activeRoomId;

  public event Action OnError;
  public event Action OnRoomJoined;

  private Thread _tcpThread;
  
  private readonly List<ILobbyFrame> _lobbyServerFrameQueue = new List<ILobbyFrame>();
  private readonly List<Action<ILobbyFrame>> _lobbyServerFrameQueueCallbacks = new List<Action<ILobbyFrame>>();

  private readonly RingBuffer _readBuffer = new RingBuffer(4096 * 16);
  private readonly RingBuffer _writeBuffer = new RingBuffer(4096 * 16);
  private Thread _mainThread;
  private List<Action> _mainThreadActionQueue = new List<Action>();
  private long _connectionFailed = 0;
  private const int MaxPeers = 63;

  private void Start() {
    instance = this;
    _mainThread = Thread.CurrentThread;
    _tcpThread = new Thread(RunTcpThread);
    _tcpThread.Start();
  }
  
  private void OnDestroy() {
    _tcpThread.Abort();
  }

  private void RunTcpThread() {
    TcpClient client = null;
    try {
      int retryAfter = 1000;
      int maxRetry = 16000;
      while (true) {
        try {
          client = new TcpClient(serverAddress, 3945);
          break;
        } catch (SocketException) {
          if (retryAfter >= maxRetry) {
            Interlocked.Exchange(ref _connectionFailed, 1);
            throw;
          }

          Thread.Sleep(retryAfter);
          retryAfter *= 2;
        }
      }

      var stream = client.GetStream();

      while (true) {
        if (stream.CanWrite) {
          lock (_lobbyServerFrameQueue) {
            foreach (var frame in _lobbyServerFrameQueue) {
              frame.Serialize(_writeBuffer);
              stream.Write(_writeBuffer.Buf, _writeBuffer.ReadHead,
                           _writeBuffer.ContiguousRemainingRead);
              _writeBuffer.AdvanceReadHead();
              if (_writeBuffer.ReadHead != _writeBuffer.WriteHead) {
                stream.Write(_writeBuffer.Buf, _writeBuffer.ReadHead,
                             _writeBuffer.ContiguousRemainingRead);
              }
            }

            _lobbyServerFrameQueue.Clear();
          }

          stream.FlushAsync();
        }

        while (stream.CanRead && stream.DataAvailable) {
          var bytesRead = stream.Read(_readBuffer.Buf, _readBuffer.WriteHead,
                                      _readBuffer.ContiguousRemainingWrite);
          _readBuffer.AdvanceWriteHead(bytesRead);
          var parsed = LobbyFrame.Parse(_readBuffer);
          if (parsed == null) {
            continue;
          }

          RunOnMainThread(() => HandleFrame(parsed));
        }

        stream.Flush();
        // Eugh. I don't like this, but it's probably fine.
        Thread.Sleep(16);
      }
    } finally {
      client?.Dispose();
    }
  }

  private void HandleFrame(ILobbyFrame parsed) {
    if (parsed is ArrayFrame arrayFrame) {
      if (arrayFrame.frames.Count < 1) {
        DispatchError();
        return;
      }
      var command = arrayFrame.frames[0] as SimpleFrame;
      if (command == null) {
        DispatchError();
        return;
      }
      switch (command.value) {
        case "joined": {
          if (arrayFrame.frames.Count != 3 ||
              !(arrayFrame.frames[1] is IntegerFrame) ||
              !(arrayFrame.frames[2] is BulkFrame)) {
            DispatchError();
            return;
          }
          var peerId = ((IntegerFrame) arrayFrame.frames[1]).value;
          OnPeerJoined(peerId, ((BulkFrame) arrayFrame.frames[2]).bytes);
          break;
        }
        case "candidates": {
          if (arrayFrame.frames.Count < 2 ||
              !(arrayFrame.frames[1] is IntegerFrame) ||
              arrayFrame.frames.Skip(2).Any(f => !(f is BulkFrame))) {
            DispatchError();
            return;
          }
          var peerId = ((IntegerFrame) arrayFrame.frames[1]).value;
          OnRemoteCandidates(peerId, arrayFrame.frames.Skip(2).Select(f => ((BulkFrame) f).bytes)
                                               .ToArray());
          break;
        }
      }
    } else if (_lobbyServerFrameQueueCallbacks.Any()) {
      _lobbyServerFrameQueueCallbacks[0](parsed);
      _lobbyServerFrameQueueCallbacks.RemoveAt(0);
    }
  }

  private static void RunOnMainThread(Action a) {
    if (Thread.CurrentThread == instance._mainThread) {
      a();
    } else {
      lock (instance._mainThreadActionQueue) {
        instance._mainThreadActionQueue.Add(a);
      }
    }
  }

  private void Update() {
    lock (_mainThreadActionQueue) {
      try {
        foreach (var action in _mainThreadActionQueue) {
          action();
        }
      } finally {
        _mainThreadActionQueue.Clear();
      }
    }
  }

  private void AddToFrameQueue(ILobbyFrame frame, Action<ILobbyFrame> responseCallback) {
    lock (_lobbyServerFrameQueue) {
      _lobbyServerFrameQueue.Add(frame);
      _lobbyServerFrameQueueCallbacks.Add(responseCallback);
    }
  }
  
  public void CreateARoom() {
    var frame = new ArrayFrame();
    IceProtocolManager.instance.InitializeAgentPool(MaxPeers);
    frame.frames.Add(new SimpleFrame("create"));
    frame.frames.Add(new IntegerFrame(MaxPeers));
    for (int i = 0; i < MaxPeers; i++) {
      frame.frames.Add(new BulkFrame(IceProtocolManager.instance.GetLocalDesc(i)));
    }
    AddToFrameQueue(frame, response => {
      if (!(response is SimpleFrame simpleFrame)) {
        DispatchError();
        return;
      }

      activeRoomId = simpleFrame.value;
      IceProtocolManager.instance.StartServer();
      DispatchRoomJoined();
    });
  }

  public void JoinRoom(string roomId) {
    IceProtocolManager.instance.InitializeAgentPool(1);
    activeRoomId = roomId;
    var frame = new ArrayFrame();
    frame.frames.Add(new SimpleFrame("join"));
    frame.frames.Add(new SimpleFrame(roomId));
    frame.frames.Add(new BulkFrame(IceProtocolManager.instance.GetLocalDesc(0)));
    AddToFrameQueue(frame, responseFrame => {
      var bulkFrame = responseFrame as BulkFrame;
      if (bulkFrame == null) {
        DispatchError();
        return;
      }
      IceProtocolManager.instance.BeginAddPeer(bulkFrame.bytes, 0, SendCandidates, DispatchError);
    });
  }

  private void SendCandidates(int peerId, byte[][] candidates) {
    var candidatesFrame = new ArrayFrame();
    candidatesFrame.frames.Add(new SimpleFrame("candidates"));
    candidatesFrame.frames.Add(new IntegerFrame(peerId));
    foreach (var candidate in candidates) {
      candidatesFrame.frames.Add(new BulkFrame(candidate));
    }
    AddToFrameQueue(candidatesFrame, ValidateFrameIsOk);
  }

  private void OnRemoteCandidates(int peerId, byte[][] candidates) {
    IceProtocolManager.instance.SetRemoteCandidates(peerId, candidates);
  }

  private void OnPeerJoined(int peerId, byte[] remoteDesc) {
    IceProtocolManager.instance.BeginAddPeer(remoteDesc, peerId, OnHostCandidatesGathered, DispatchError);
  }
  
  private static void OnHostCandidatesGathered(int peerId, byte[][] candidates) {
    instance.SendCandidates(peerId, candidates);
  }

  private void DispatchError() {
    OnError?.Invoke();
  }

  private void DispatchRoomJoined() {
    OnRoomJoined?.Invoke();
  }

  private static void ValidateFrameIsOk(ILobbyFrame frame) {
    if (!(frame is SimpleFrame)) {
      instance.DispatchError();
    }

    if ((frame as SimpleFrame)?.value != "ok") {
      instance.DispatchError();
    }
  }
}