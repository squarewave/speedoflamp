﻿using Unity.Collections;

public struct NetcodeMessageHeader {
  public int seqNo;
  public int size;
  public NetcodeMessageId messageTypeId;

  public static int headerSize = sizeof(int) + sizeof(int) + sizeof(NetcodeMessageId);
}