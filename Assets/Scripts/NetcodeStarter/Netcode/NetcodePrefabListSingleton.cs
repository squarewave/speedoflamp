﻿using System;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;

public class NetcodePrefabListSingleton : MonoBehaviour, IConvertGameObjectToEntity {
  public List<GameObject> prefabs;
  public List<GameObject> deathPrefabs;
  public static NetcodePrefabListSingleton instance;

  private void Awake() {
    instance = this;
  }

  public void Convert(Entity entity,
                      EntityManager dstManager,
                      GameObjectConversionSystem conversionSystem) {
    var collection = new NetcodePrefabList();
    var prefabList = new NativeList<NetcodePrefabBuffer>(Allocator.Temp);
    foreach (GameObject prefab in prefabs) {
      var prefabEntity =
        GameObjectConversionUtility.ConvertGameObjectHierarchy(
          prefab, conversionSystem.ForkSettings(1));
      prefabList.Add(new NetcodePrefabBuffer {value = prefabEntity});
    }
    
    collection.prefabs = conversionSystem.CreateAdditionalEntity(this);
    var prefabBuffer = dstManager.AddBuffer<NetcodePrefabBuffer>(collection.prefabs);

    foreach (NetcodePrefabBuffer prefabBuf in prefabList) {
      prefabBuffer.Add(prefabBuf);
    }
    
    var deathPrefabList = new NativeList<NetcodePrefabBuffer>(Allocator.Temp);
    foreach (GameObject prefab in deathPrefabs) {
      var prefabEntity =
        GameObjectConversionUtility.ConvertGameObjectHierarchy(
          prefab, conversionSystem.ForkSettings(1));
      deathPrefabList.Add(new NetcodePrefabBuffer {value = prefabEntity});
    }
    
    collection.deathPrefabs = conversionSystem.CreateAdditionalEntity(this);
    var deathPrefabBuffer = dstManager.AddBuffer<NetcodePrefabBuffer>(collection.deathPrefabs);

    foreach (NetcodePrefabBuffer prefabBuf in deathPrefabList) {
      deathPrefabBuffer.Add(prefabBuf);
    }

    dstManager.AddComponentData(entity, collection);
  }
}
