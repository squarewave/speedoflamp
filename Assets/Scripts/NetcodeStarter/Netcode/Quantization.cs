﻿using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;

public static class Quantization {
  private const int QuaternionComponentBits = 14;
  private const double MaxQuaternionComponentValue = 0.707107;
  private const double MinQuaternionComponentValue = -MaxQuaternionComponentValue;
  public const int QuaternionBits = 2 + QuaternionComponentBits * 3;

  public static int BitsForValue(long size) {
    int bits = 0;
    while (1L << bits < size) {
      bits++;
    }

    return bits;
  }

  private static bool WriteBits(uint value,
                                NativeArray<byte> bytes,
                                ref int bitOffset,
                                int byteOffsetMask,
                                int numBits) {
    int byteOffset = (bitOffset >> 3) & byteOffsetMask;
    if (byteOffset > bytes.Length) {
      return false;
    }

    int firstBitOffset = bitOffset & 0b111;
    int numBitsInFirstByte = 8 - firstBitOffset;
    if (numBitsInFirstByte > numBits) {
      numBitsInFirstByte = numBits;
    }
    byte existingBitsMask = (byte) ((1L << firstBitOffset) - 1);
    bytes[byteOffset] &= existingBitsMask;
    bytes[byteOffset] |= (byte) ((value << firstBitOffset) & 0xff);
    int bitsRemaining = numBits - numBitsInFirstByte;
    value >>= numBitsInFirstByte;
    while (bitsRemaining > 0) {
      byteOffset = (byteOffset + 1) & byteOffsetMask;
      if (byteOffset > bytes.Length) {
        return false;
      }

      bytes[byteOffset] = (byte) (value & 0xff);
      value >>= 8;
      bitsRemaining -= 8;
    }

    bitOffset += numBits;
    return true;
  }

  private static bool ReadBits(NativeArray<byte> bytes,
                               ref int bitOffset,
                               int byteOffsetMask,
                               int numBits,
                               out uint quantized) {
    quantized = 0;
    int byteOffset = (bitOffset >> 3) & byteOffsetMask;
    if (byteOffset > bytes.Length) {
      return false;
    }

    int firstBitOffset = bitOffset & 0b111;
    int numBitsInFirstByte = 8 - firstBitOffset;
    if (numBitsInFirstByte > numBits) {
      numBitsInFirstByte = numBits;
    }
    quantized |= (uint) (bytes[byteOffset] >> firstBitOffset) & ((1u << numBitsInFirstByte) - 1u);
    int bitsUsed = numBitsInFirstByte;
    while (bitsUsed < numBits) {
      byteOffset = (byteOffset + 1) & byteOffsetMask;
      if (byteOffset > bytes.Length) {
        return false;
      }

      int bitsRemaining = numBits - bitsUsed;
      quantized |= (bytes[byteOffset] & ((1u << bitsRemaining) - 1u)) << bitsUsed;
      bitsUsed += 8;
    }

    bitOffset += numBits;
    return true;
  }

  public static bool Quantize(float value,
                              NativeArray<byte> bytes,
                              ref int bitOffset,
                              int byteOffsetMask,
                              int numBits,
                              double min,
                              double max) {
    double valueD = value;
    double normalized = math.clamp((valueD - min) / (max - min), 0.0, 1.0);
    return WriteBits((uint) (normalized * ((1L << numBits) - 1) + .5), bytes, ref bitOffset,
                     byteOffsetMask, numBits);
  }

  public static bool Quantize(int value,
                              NativeArray<byte> bytes,
                              ref int bitOffset,
                              int byteOffsetMask,
                              int numBits,
                              long min,
                              long max) {
    if (numBits == -1) {
      numBits = BitsForValue(max - min);
    }
    if (max - min > 1L << numBits) {
      Debug.LogError("Mismatch between min and max ranges and numBits");
      return false;
    }
    if (value < min || value >= max) {
      Debug.LogError($"Tried to quantize an integer outside the acceptable range (value: {value}, min: {min}, max: {max})");
      return false;
    }
    
    return WriteBits((uint)(value - min), bytes, ref bitOffset,
                     byteOffsetMask, numBits);
  }

  public static bool Quantize(bool value,
                              NativeArray<byte> bytes,
                              ref int bitOffset,
                              int byteOffsetMask) {
    return WriteBits(value ? 1u : 0, bytes, ref bitOffset,
                     byteOffsetMask, 1);
  }
  public static bool Dequantize(NativeArray<byte> bytes,
                                ref int bitOffset,
                                int byteOffsetMask,
                                int numBits,
                                double min,
                                double max,
                                out float result) {
    if (!ReadBits(bytes, ref bitOffset, byteOffsetMask, numBits, out uint quantized)) {
      result = 0f;
      return false;
    }

    var a = quantized - .5;
    long b = 1L << numBits;
    var normalized = a / (b - 1);
    result = (float) (normalized * (max - min) + min);
    return true;
  }

  public static bool Dequantize(NativeArray<byte> bytes,
                                ref int bitOffset,
                                int byteOffsetMask,
                                int numBits,
                                long min,
                                long max,
                                out int result) {
    if (numBits == -1) {
      numBits = BitsForValue(max - min);
    }
    
    if (!ReadBits(bytes, ref bitOffset, byteOffsetMask, numBits, out uint quantized)) {
      result = 0;
      return false;
    }

    result = (int)(quantized + min);
    return true;
  }

  public static bool Dequantize(NativeArray<byte> bytes,
                                ref int bitOffset,
                                int byteOffsetMask,
                                out bool result) {
    if (!ReadBits(bytes, ref bitOffset, byteOffsetMask, 1, out uint quantized)) {
      result = false;
      return false;
    }

    result = quantized != 0;
    return true;
  }

  public static bool QuantizeQuaternion(quaternion quat,
                                        NativeArray<byte> bytes,
                                        ref int bitOffset,
                                        int byteOffsetMask) {
    int largest = 0;
    for (int i = 1; i < 4; i++) {
      if (math.abs(quat.value[i]) > math.abs(quat.value[largest])) {
        largest = i;
      }
    }

    float sign = math.sign(quat.value[largest]);
    if (!WriteBits((uint) largest & 0x3, bytes, ref bitOffset, byteOffsetMask, 2)) {
      return false;
    }

    for (int i = 0; i < 4; i++) {
      if (i != largest) {
        if (!Quantize(quat.value[i] * sign, bytes, ref bitOffset, byteOffsetMask,
                      QuaternionComponentBits,
                      MinQuaternionComponentValue,
                      MaxQuaternionComponentValue)) {
          return false;
        }
      }
    }

    return true;
  }

  public static bool DequantizeQuaternion(NativeArray<byte> bytes,
                                          ref int bitOffset,
                                          int byteOffsetMask,
                                          out quaternion result) {
    result = quaternion.identity;
    
    if (!ReadBits(bytes, ref bitOffset, byteOffsetMask, 2, out var largest)) {
      return false;
    }

    float largestSq = 1f;
    for (int i = 0; i < 4; i++) {
      if (i != largest) {
        if (!Dequantize(bytes, ref bitOffset, byteOffsetMask,
                        QuaternionComponentBits,
                        MinQuaternionComponentValue,
                        MaxQuaternionComponentValue,
                        out var component)) {
          return false;
        }

        result.value[i] = component;
        largestSq -= result.value[i] * result.value[i];
      }
    }

    result.value[(int) largest] = math.sqrt(largestSq);
    return true;
  }

  public static bool Quantize(float3 value,
                              NativeArray<byte> bytes,
                              ref int bitOffset,
                              int byteOffsetMask,
                              int3 bits,
                              double3 min,
                              double3 max) {
    return
      Quantize(value.x, bytes, ref bitOffset, byteOffsetMask, bits.x, min.x, max.x) &&
      Quantize(value.y, bytes, ref bitOffset, byteOffsetMask, bits.y, min.y, max.y) &&
      Quantize(value.z, bytes, ref bitOffset, byteOffsetMask, bits.z, min.z, max.z);
  }

  public static bool Dequantize(NativeArray<byte> bytes,
                                ref int bitOffset,
                                int byteOffsetMask,
                                int3 bits,
                                double3 min,
                                double3 max,
                                out float3 result) {
    result = float3.zero;
    return
      Dequantize(bytes, ref bitOffset, byteOffsetMask, bits.x, min.x, max.x, out result.x) &&
      Dequantize(bytes, ref bitOffset, byteOffsetMask, bits.y, min.y, max.y, out result.y) &&
      Dequantize(bytes, ref bitOffset, byteOffsetMask, bits.z, min.z, max.z, out result.z);
  }

  public static bool Quantize(float2 value,
                              NativeArray<byte> bytes,
                              ref int bitOffset,
                              int byteOffsetMask,
                              int2 bits,
                              double2 min,
                              double2 max) {
    return
      Quantize(value.x, bytes, ref bitOffset, byteOffsetMask, bits.x, min.x, max.x) &&
      Quantize(value.y, bytes, ref bitOffset, byteOffsetMask, bits.y, min.y, max.y);
  }

  public static bool Dequantize(NativeArray<byte> bytes,
                                ref int bitOffset,
                                int byteOffsetMask,
                                int2 bits,
                                double2 min,
                                double2 max,
                                out float2 result) {
    result = float2.zero;
    return
      Dequantize(bytes, ref bitOffset, byteOffsetMask, bits.x, min.x, max.x, out result.x) &&
      Dequantize(bytes, ref bitOffset, byteOffsetMask, bits.y, min.y, max.y, out result.y);
  }
}
