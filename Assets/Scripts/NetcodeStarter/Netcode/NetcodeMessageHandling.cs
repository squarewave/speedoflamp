﻿using System;
using Unity.Collections;
using Unity.Entities;

public static partial class NetcodeMessageHandling {
  public static bool HandleMessage(Object world,
                                   bool isServer,
                                   NetcodeMessageHeader header,
                                   int playerId,
                                   NativeArray<byte> buffer,
                                   ref int offset) {
    throw new NotImplementedException();
  }
  
  public static bool HandleMessage<T>(ref T message,
                                      World world,
                                      bool isServer,
                                      int playerId,
                                      NativeArray<byte> buffer,
                                      ref int offset) where T : INetcodeMessage<T> {
    var skipDispose = false;
    var success = message.Process(world, playerId, isServer, ref skipDispose);

    if (success) {
      foreach (var listener in ListenersByType<T>.OneTimeListeners) {
        listener.Invoke(message);
      }

      foreach (var toRemove in ListenersByType<T>.PersistentListenersToRemove) {
        ListenersByType<T>.PersistentListeners.Remove(toRemove);
      }
      ListenersByType<T>.PersistentListenersToRemove.Clear();
      
      foreach (var listener in ListenersByType<T>.PersistentListeners) {
        listener.Invoke(message);
      }
      
      foreach (var toRemove in ListenersByType<T>.PersistentListenersToRemove) {
        ListenersByType<T>.PersistentListeners.Remove(toRemove);
      }
      ListenersByType<T>.PersistentListenersToRemove.Clear();
    }
    
    if (!skipDispose) {
      message.Dispose();
    }

    ListenersByType<T>.OneTimeListeners.Clear();

    return success;
  }
}

public static partial class NetcodeMessageSerialization {
  public static bool Serialize<T>(T msg,
                               NativeArray<byte> buffer,
                               ref int bitOffset,
                               int bufferMask) {
    throw new NotImplementedException();
  }

  public static bool Deserialize<T>(NativeArray<byte> bytes, ref int bitOffset, Allocator allocator, int bufferMask, out T generic) {
    throw new NotImplementedException();
  }
}
