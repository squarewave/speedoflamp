﻿using Unity.Entities;

public struct NetcodeObject : IComponentData {
  public const int UnassignedPlayerId = 0;
  public const int UnassignedPrefabIndex = -1;
  
  public int prefabIndex;
  public int id;
  public int playerId;
  public bool isDeathPrefab;
}