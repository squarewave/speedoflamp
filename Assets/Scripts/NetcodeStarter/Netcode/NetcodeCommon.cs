﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Entities;
using UnityEngine;
using Random = UnityEngine.Random;

public class NetcodeSendBuffer {
  public int delivered;
  public int timesSent;
  public int seqNo;
  public int playerId;
  public int size;
  public readonly byte[] buffer = new byte[NetcodeCommon.MaxPacketSize];
  public NetcodeMessageId messageTypeId;
}

public class NetcodeChannel {
  public int playerId;
  public readonly HashSet<int> pendingAcks = new HashSet<int>();

  public NativeArray<byte> pendingPacketRingBuffer =
    new NativeArray<byte>(NetcodeCommon.PendingPacketRingBufferSize, Allocator.Persistent);

  public NativeArray<PacketMeta> pendingPacketMetaRingBuffer =
    new NativeArray<PacketMeta>(NetcodeCommon.PendingPacketRingBufferItemCount,
                                Allocator.Persistent);

  public readonly HashSet<int> seenMessages = new HashSet<int>();
  public readonly List<OutOfOrderMessage> outOfOrderMessages = new List<OutOfOrderMessage>();

  public int pendingPackedRingBufferReadItem;
  public int pendingPackedRingBufferWriteItem;
  public readonly object pendingPacketRingBufferLock = new object();
  public int nextSendSeqNo;
  public int nextSendUnreliableSeqNo;
  public int nextRecvSeqNo;
}

public struct OutOfOrderMessage {
  public NetcodeMessageHeader header;
  public NativeArray<byte> bytes;
}

public abstract class NetcodeCommon : IDisposable {
  protected readonly Socket socket =
    new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

  protected readonly NetcodeChannel toServerChannel = new NetcodeChannel();

  public const int ServerPlayerId = 1;
  public const int MaxAcks = 16;
  public const int OneAckPaddingAssumption = 8;
  public const int AggregateSeqNo = int.MinValue;

  // Note: this and PendingPacketRingBufferItemCount must be powers of 2 to use masking for a ring
  // buffer implementation.
  public const int MaxPacketSize = 1 << 10;
  public const int PendingPacketRingBufferItemCount = 1 << 8;
  public const int PendingPacketRingBufferItemMask = PendingPacketRingBufferItemCount - 1;
  public const int PendingPacketRingBufferSize = PendingPacketRingBufferItemCount * MaxPacketSize;
  public const int PendingPacketRingBufferMask = PendingPacketRingBufferSize - 1;

  // Network debug values - generally speaking, please keep these at 0
  private const float ArtificialPacketDelay = 0f;

  private const float PacketDropRate = 0f;

  // TODO: simulating packets arriving out of order is a bit more complicated, but we should try to
  // do so at some point.
  private readonly NativeArray<bool> _packetDropRateTable;

  public readonly int[] recentMessageSizes = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  public int recentMessageSizesIndex = 0;
  public float lastMessageSent;
  public float rollingAverageBytesPerSecond;

  protected readonly List<NetcodeSendBuffer> _buffers = new List<NetcodeSendBuffer>();

  protected World world;

  protected bool isServer;
  private float _lastTickTime;

  private bool IsReliableMessage(int seqNo) {
    return seqNo >= 0;
  }

  protected NetcodeCommon() {
    if (PacketDropRate > 0) {
      _packetDropRateTable =
        new NativeArray<bool>(PendingPacketRingBufferItemCount, Allocator.Persistent);
      for (int i = 0; i < PendingPacketRingBufferItemCount; i++) {
        _packetDropRateTable[i] = Random.value > PacketDropRate;
      }
    }
    toServerChannel.playerId = ServerPlayerId;
  }
  
  public void AddOneTimeListener<T>(Action<T> action) {
    ListenersByType<T>.OneTimeListeners.Add(action);
  }
  
  public void RemoveOneTimeListener<T>(Action<T> action) {
    ListenersByType<T>.OneTimeListeners.Remove(action);
  }
  
  public void AddPersistentListener<T>(Action<T> action) {
    ListenersByType<T>.PersistentListeners.Add(action);
  }
  
  public void RemovePersistentListener<T>(Action<T> action) {
    ListenersByType<T>.PersistentListenersToRemove.Add(action);
  }
  
  public void QueueSendToServer<T>(T message) where T : INetcodeMessage<T> {
    QueueMessageInternal(message, toServerChannel); 
  }

  public abstract void ReceiveCallback(IntPtr peer, IntPtr buf, int size);

  protected NetcodeSendBuffer GetOrCreateSendBuffer(int playerId,
                                                    int seqNo,
                                                    NetcodeMessageId messageTypeId) {
    NetcodeSendBuffer buffer = null;
    foreach (NetcodeSendBuffer sendBuffer in _buffers) {
      if (Interlocked.Exchange(ref sendBuffer.delivered, 0) != 0) {
        buffer = sendBuffer;
        break;
      }
    }

    if (buffer == null) {
      _buffers.Add(buffer = new NetcodeSendBuffer {delivered = 0});
    }

    buffer.playerId = playerId;
    buffer.seqNo = seqNo;
    buffer.timesSent = 0;
    buffer.messageTypeId = messageTypeId;

    return buffer;
  }

  protected unsafe void QueueMessageInternal<T>(T message, NetcodeChannel channel)
    where T : INetcodeMessage<T> {
    NetcodeSendBuffer buffer =
      GetOrCreateSendBuffer(channel.playerId,
                            message.IsReliable()
                              ? channel.nextSendSeqNo++
                              : --channel.nextSendUnreliableSeqNo,
                            NetcodeMessageHandling.MessageIdForType<T>());

    fixed (byte* rawBytes = buffer.buffer) {
      NativeArray<byte> native =
        NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<byte>(
          rawBytes, buffer.buffer.Length, Allocator.None);
      int offset = 0;
#if ENABLE_UNITY_COLLECTIONS_CHECKS
      NativeArrayUnsafeUtility.SetAtomicSafetyHandle(ref native,
                                                     AtomicSafetyHandle
                                                       .GetTempUnsafePtrSliceHandle());
#endif
      var success = NetcodeMessageStatic.SerializeMessage(native,
                                                          buffer.seqNo,
                                                          message,
                                                          out buffer.size,
                                                          ref offset,
                                                          NativeArrayUtils.DefaultBufferMask);
      if (!success || buffer.size > MaxPacketSize - OneAckPaddingAssumption) {
        Interlocked.Exchange(ref buffer.delivered, 1);
        Debug.LogError("Message too large to fit!");
      }
    }
  }

  protected void SortBuffers() {
    // Sort the buffers just to try to not make the receiver do too much work reordering them
    _buffers.Sort((lhs, rhs) => {
      if (lhs.timesSent != rhs.timesSent) {
        return rhs.timesSent - lhs.timesSent;
      }

      return lhs.seqNo - rhs.seqNo;
    });
  }

  protected void ProcessIncomingMessages(NetcodeChannel channel) {
    lock (channel.pendingPacketRingBufferLock) {
      _lastTickTime = Time.time;
      float timeGate = ArtificialPacketDelay > 0
        ? Time.time - ArtificialPacketDelay
        : float.MaxValue;
      while (channel.pendingPackedRingBufferReadItem <
             channel.pendingPackedRingBufferWriteItem &&
             channel.pendingPacketMetaRingBuffer[
                      channel.pendingPackedRingBufferReadItem & PendingPacketRingBufferItemMask]
                    .timeReceived <= timeGate) {
        int readItem = channel.pendingPackedRingBufferReadItem & PendingPacketRingBufferItemMask;
        if (PacketDropRate > 0 && !_packetDropRateTable[readItem]) {
          channel.pendingPackedRingBufferReadItem++;
          continue;
        }

        int offset = readItem * MaxPacketSize;
        int numAcks = 0;
        using (var acks = new NativeHashMap<int, bool>(MaxAcks, Allocator.Temp)) {
          NativeArrayUtils.Read(channel.pendingPacketRingBuffer, ref offset, out numAcks,
                                PendingPacketRingBufferMask);
          for (int i = 0; i < numAcks; i++) {
            NativeArrayUtils.Read(channel.pendingPacketRingBuffer, ref offset, out int acked,
                                  PendingPacketRingBufferMask);
            acks.TryAdd(acked, true);
          }

          foreach (NetcodeSendBuffer sendBuffer in _buffers) {
            if (channel.playerId == sendBuffer.playerId &&
                acks.ContainsKey(sendBuffer.seqNo)) {
              Interlocked.Exchange(ref sendBuffer.delivered, 1);
            }
          }
        }
        
        NativeArrayUtils.Read(channel.pendingPacketRingBuffer, ref offset,
                              out int numSubpackets,
                              PendingPacketRingBufferMask);
        for (int i = 0; i < numSubpackets; i++) {
          var startOffset = offset;
          if (!NetcodeMessageStatic.DeserializeMessageHeader(channel.pendingPacketRingBuffer,
                                                             out var header,
                                                             ref offset,
                                                             PendingPacketRingBufferMask)) {
            break;
          }

          if (((startOffset + header.size) & PendingPacketRingBufferMask) <
              (startOffset & PendingPacketRingBufferMask) ||
              header.size > MaxPacketSize - OneAckPaddingAssumption) {
            Debug.LogError("Bad message size in header - exceeds packet size");
            break;
          }

          if (channel.seenMessages.Contains(header.seqNo)) {
            offset = startOffset + header.size;
            continue;
          }

          channel.seenMessages.Add(header.seqNo);

          if (IsReliableMessage(header.seqNo) && header.seqNo != channel.nextRecvSeqNo) {
            Debug.Log(
              $"Deferring msg #{header.seqNo}, type {header.messageTypeId} (msg #{channel.nextRecvSeqNo} expected)");
            var bytes = new NativeArray<byte>(header.size, Allocator.Persistent);
            bytes.CopyFrom(
              channel.pendingPacketRingBuffer.GetSubArray(startOffset & PendingPacketRingBufferMask,
                                                          header.size));
            channel.outOfOrderMessages.Add(new OutOfOrderMessage {
              header = header, bytes = bytes
            });
            channel.outOfOrderMessages.Sort((lhs, rhs) => lhs.header.seqNo - rhs.header.seqNo);
            offset = startOffset + header.size;
          } else {
            if (IsReliableMessage(header.seqNo)) {
#if NETCODE_DEBUG_LOGGING
              Debug.Log($"Processing msg #{header.seqNo}, type {header.messageTypeId}");
#endif
            }

            var success = HandleMessage(header, channel.playerId, channel.pendingPacketRingBuffer,
                                        ref offset);
            if (!success) {
              Debug.LogError($"Failed processing message #{header.seqNo} ({header.messageTypeId})");
            }

            if (IsReliableMessage(header.seqNo)) {
              channel.pendingAcks.Add(header.seqNo);

              AdvanceExpectedSeqNo(channel);
            }
          }
        }

        channel.pendingPackedRingBufferReadItem++;
      }
    }
  }

  private void AdvanceExpectedSeqNo(NetcodeChannel channel) {
    channel.nextRecvSeqNo++;

    if (channel.outOfOrderMessages.Count > 0 &&
        channel.outOfOrderMessages[0].header.seqNo == channel.nextRecvSeqNo) {
      OutOfOrderMessage message = channel.outOfOrderMessages[0];
      Debug.Log(
        $"Processing out of order message {message.header.seqNo} {message.header.messageTypeId} {message.header.size}");
      int offset = 0;
      var success = HandleMessage(message.header,
                                  channel.playerId,
                                  message.bytes,
                                  ref offset);
      if (!success) {
        Debug.LogError("Failed to process out of order message.");
        return;
      }

      channel.pendingAcks.Add(message.header.seqNo);
      message.bytes.Dispose();
      AdvanceExpectedSeqNo(channel);
    }
  }

  protected unsafe void SendAcksAndQueuedMessages(NetcodeChannel channel, IntPtr peer) {
    var aggregateBuffer =
      GetOrCreateSendBuffer(channel.playerId, AggregateSeqNo, NetcodeMessageId.Count);
    aggregateBuffer.seqNo = AggregateSeqNo;

    int numAcks = 0;
    int numAggregated = 0;
    int offset = 0;

    fixed (byte* rawBytes = aggregateBuffer.buffer) {
      NativeArray<byte> native =
        NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<byte>(
          rawBytes, aggregateBuffer.buffer.Length, Allocator.None);
#if ENABLE_UNITY_COLLECTIONS_CHECKS
      NativeArrayUnsafeUtility.SetAtomicSafetyHandle(ref native,
                                                     AtomicSafetyHandle
                                                       .GetTempUnsafePtrSliceHandle());
#endif
      NativeArrayUtils.Write(native, ref offset, channel.pendingAcks.Count);
      foreach (int ack in channel.pendingAcks) {
        if (numAcks + 1 > MaxAcks) {
          Debug.LogError("Hit max acks");
          break;
        }

        numAcks++;
        if (!NativeArrayUtils.Write(native, ref offset, ack)) {
          break;
        }
      }

      channel.pendingAcks.Clear();

      int numAggregatedOffset = offset;
      NativeArrayUtils.Write(native, ref offset, 0);
      foreach (NetcodeSendBuffer sendBuffer in _buffers) {
        if (sendBuffer.delivered == 0 && sendBuffer.seqNo != AggregateSeqNo &&
            sendBuffer.playerId == channel.playerId) {
          if (offset + sendBuffer.size < MaxPacketSize) {
            fixed (byte* dest = aggregateBuffer.buffer)
            fixed (byte* source = sendBuffer.buffer) {
              UnsafeUtility.MemCpy(dest + offset,
                                   source,
                                   sendBuffer.size);
              offset += sendBuffer.size;
            }

            numAggregated++;
            sendBuffer.timesSent++;
            if (!IsReliableMessage(sendBuffer.seqNo)) {
              Interlocked.Exchange(ref sendBuffer.delivered, 1);
            } else {
              if (IsReliableMessage(sendBuffer.seqNo)) {
#if NETCODE_DEBUG_LOGGING
                Debug.Log($"Sending msg #{sendBuffer.seqNo}, type {sendBuffer.messageTypeId}");
#endif
              }
            }
          } else {
            Debug.LogError("Packet cannot fit all messages.");
          }
        }
      }

      NativeArrayUtils.Write(native, ref numAggregatedOffset, numAggregated);
    }

    if (numAcks > 0 || numAggregated > 0) {
      if (isServer && channel == toServerChannel) {
        fixed (byte* src = &aggregateBuffer.buffer[0]) {
          CopyPacketToRingBufferOmt(channel, src, aggregateBuffer.buffer.Length);
        }
        Interlocked.Exchange(ref aggregateBuffer.delivered, 1);
      } else {
        recentMessageSizes[recentMessageSizesIndex++ % recentMessageSizes.Length] = offset;
        var deltaTime = Time.unscaledTime - lastMessageSent;
        lastMessageSent = Time.unscaledTime;
        rollingAverageBytesPerSecond +=
          (offset / deltaTime - rollingAverageBytesPerSecond) * deltaTime * .5f;
        IceProtocolManager.Send(peer, aggregateBuffer.buffer, offset);
        Interlocked.Exchange(ref aggregateBuffer.delivered, 1);
      }
    } else {
      Interlocked.Exchange(ref aggregateBuffer.delivered, 1);
    }
  }

  // Note on abbreviations: "Omt" means "off main thread"
  protected unsafe void CopyPacketToRingBufferOmt(NetcodeChannel channel, byte* buffer, int size) {
    if (size > MaxPacketSize) {
      Debug.LogError("Unexpected packet length");
      return;
    }

    lock (channel.pendingPacketRingBufferLock) {
      int itemIndex = channel.pendingPackedRingBufferWriteItem & PendingPacketRingBufferItemMask;
      if (PacketDropRate > 0 && !_packetDropRateTable[itemIndex]) {
        channel.pendingPackedRingBufferWriteItem++;
        return;
      }

      var offset = itemIndex * MaxPacketSize;
      if (offset + size > PendingPacketRingBufferSize) {
        var buf0 =
          channel.pendingPacketRingBuffer.GetSubArray(offset, PendingPacketRingBufferSize - offset);
        var buf1 =
          channel.pendingPacketRingBuffer.GetSubArray(
            0, (offset + size) & PendingPacketRingBufferMask);
        UnsafeUtility.MemCpy(buf0.GetUnsafePtr(), buffer, buf0.Length);
        UnsafeUtility.MemCpy(buf1.GetUnsafePtr(), buffer + buf0.Length, buf1.Length);
      } else {
        var buf = channel.pendingPacketRingBuffer.GetSubArray(offset, size);
        UnsafeUtility.MemCpy(buf.GetUnsafePtr(), buffer, size);
      }

      channel.pendingPacketMetaRingBuffer[
        itemIndex] = new PacketMeta {
        timeReceived = _lastTickTime,
      };

      channel.pendingPackedRingBufferWriteItem++;
    }
  }

  private bool HandleMessage(NetcodeMessageHeader header,
                             int playerId,
                             NativeArray<byte> buffer,
                             ref int offset) {
    return NetcodeMessageHandling.HandleMessage(world, isServer, header, playerId, buffer,
                                                ref offset);
  }

  public void Dispose() { }
}

public static class ListenersByType<T> {
  public static readonly List<Action<T>> OneTimeListeners = new List<Action<T>>();
  public static readonly List<Action<T>> PersistentListeners = new List<Action<T>>();
  public static readonly List<Action<T>> PersistentListenersToRemove = new List<Action<T>>();
}

public struct PacketMeta {
  public float timeReceived;
}
