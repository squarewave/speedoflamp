﻿#if UNITY_EDITOR
using System;
using System.Linq;
using System.Reflection;
using Unity.Collections;
using Unity.Mathematics;

public static class MessageCodeGeneration {
  public static string GenerateCode() {
    var assembly = Assembly.GetExecutingAssembly();
    var messageTypes = assembly.GetTypes().Where(
      t => t.GetCustomAttributes(
              typeof(GenerateMessageSerializationAttribute), true)
            .Length > 0).ToList();
    var fieldTypes = assembly.GetTypes().Where(
      t => t.GetCustomAttributes(
              typeof(GenerateMessageFieldSerializationAttribute), true)
            .Length > 0);

    var serializables = messageTypes.Concat(fieldTypes).ToList();
    var messageSerializers = serializables.Select(GetMessageSerializationCode);
    var messageDeserializers = serializables.Select(GetMessageDeserializationCode);

    return $@"using Unity.Collections;
using Unity.Entities;
using UnityEngine;

public enum NetcodeMessageId {{
  Null,
  {string.Join(",\n  ", messageTypes.Select(t => t.Name))},
  Count,
}}

public static partial class NetcodeMessageHandling {{
  public static NetcodeMessageId MessageIdForType<T>() {{
    {string.Join("\n    ", messageTypes.Select(t => $"if (typeof(T) == typeof({t.FullName})) {{ return NetcodeMessageId.{t.Name}; }}"))}
    return NetcodeMessageId.Null;
  }}

  public static bool HandleMessage(World world,
                                   bool isServer,
                                   NetcodeMessageHeader header,
                                   int playerId,
                                   NativeArray<byte> buffer,
                                   ref int offset) {{
    switch (header.messageTypeId) {{
       {string.Join("", messageTypes.Select(GetHandleMessageCaseStatement))}
    }}

    return false;
  }}
}}

public static partial class NetcodeMessageSerialization {{
{string.Join("", messageSerializers)}
{string.Join("", messageDeserializers)}
}}
";
  }

  private static string GetHandleMessageCaseStatement(Type type) {
    return
      $@"
      case NetcodeMessageId.{type.Name}: {{
        int startOffset = offset;
        int bitOffset = offset * 8;
        if (!NetcodeMessageSerialization.Deserialize(buffer, ref bitOffset, Allocator.Persistent,
                                                     NetcodeCommon.PendingPacketRingBufferMask,
                                                     out {type.Name} message)) {{
          Debug.LogError(""Failed to deserialize message of type "" + header.messageTypeId);
          return false;
        }}
        offset = bitOffset / 8 + (bitOffset % 8 != 0 ? 1 : 0);
        if (offset - startOffset + NetcodeMessageHeader.headerSize != header.size) {{
          Debug.LogError(
            $""Incorrectly sized message (deserialized: {{offset - startOffset + NetcodeMessageHeader.headerSize}} header-reported: {{header.size}}) (type: {{header.messageTypeId}})"");
        }}
        return HandleMessage<{type.Name}>(ref message, world, isServer, playerId, buffer, ref offset);
      }}";
  }

  private static string GetMessageDeserializationCode(Type type) {
    var deserializationCode = type.GetFields(BindingFlags.Instance | BindingFlags.Public)
                                  .Select(GetFieldDeserializationCode);
    return $@"
  public static bool Deserialize(NativeArray<byte> buffer,
                                 ref int bitOffset,
                                 Allocator allocator,
                                 int bufferMask,
                                 out {type.FullName} msg) {{
    msg = default;
{string.Join("", deserializationCode)}

    return true;
  }}";
  }

  private static string GetMessageSerializationCode(Type type) {
    var serializationCode = type.GetFields(BindingFlags.Instance | BindingFlags.Public)
                                .Select(GetFieldSerializationCode);
    return $@"
  public static bool Serialize({type.FullName} msg,
                               NativeArray<byte> buffer,
                               ref int bitOffset,
                               int bufferMask) {{
{string.Join("", serializationCode)}

    return true;
  }}";
  }

  private static string GetFieldDeserializationCode(FieldInfo arg) {
    if (arg.FieldType == typeof(int) ||
        arg.FieldType == typeof(float) ||
        arg.FieldType == typeof(float2) ||
        arg.FieldType == typeof(float3)) {
      return GetQuantizedDeserializationCode(arg);
    } else if (arg.FieldType == typeof(quaternion)) {
      return GetQuaternionDeserializationCode(arg);
    } else if (arg.FieldType == typeof(bool)) {
      return GetBoolDeserializationCode(arg);
    } else if (arg.FieldType.IsEnum) {
      return GetEnumDeserializationCode(arg);
    } else if (arg.FieldType.IsConstructedGenericType &&
               arg.FieldType.GetGenericTypeDefinition() == typeof(NativeArray<>)) {
      return GetNativeArrayDeserializationCode(arg);
    } else if (HasFieldSerializationAttribute(arg.FieldType)) {
      return GetUserStructDeserializationCode(arg);
    }

    throw new ArgumentException($"Type not supported: {arg.FieldType}");
  }

  private static string GetFieldSerializationCode(FieldInfo arg) {
    if (arg.FieldType == typeof(int) ||
        arg.FieldType == typeof(float) ||
        arg.FieldType == typeof(float2) ||
        arg.FieldType == typeof(float3)) {
      return GetQuantizedSerializationCode(arg);
    } else if (arg.FieldType == typeof(quaternion)) {
      return GetQuaternionSerializationCode(arg);
    } else if (arg.FieldType == typeof(bool)) {
      return GetBoolSerializationCode(arg);
    } else if (arg.FieldType.IsEnum) {
      return GetEnumSerializationCode(arg);
    } else if (arg.FieldType.IsConstructedGenericType &&
               arg.FieldType.GetGenericTypeDefinition() == typeof(NativeArray<>)) {
      return GetNativeArraySerializationCode(arg);
    } else if (HasFieldSerializationAttribute(arg.FieldType)) {
      return GetUserStructSerializationCode(arg);
    }

    throw new ArgumentException($"Type not supported: {arg.FieldType}");
  }

  private static string GetUserStructSerializationCode(FieldInfo fieldInfo) {
    return $@"
    if (!Serialize(msg.{fieldInfo.Name}, buffer, ref bitOffset, bufferMask)) {{
      return false;
    }}";
  }

  private static string GetUserStructDeserializationCode(FieldInfo fieldInfo) {
    return $@"
    if (!Deserialize(buffer, ref bitOffset, allocator, bufferMask, out {fieldInfo.FieldType.FullName} {fieldInfo.Name}Value)) {{
      return false;
    }}
    msg.{fieldInfo.Name} = {fieldInfo.Name}Value;";
  }

  private static string GetNativeArraySerializationCode(FieldInfo fieldInfo) {
    return $@"
    if (!Quantization.Quantize(msg.{fieldInfo.Name}.Length, buffer, ref bitOffset, bufferMask, 16, 0, 1 << 16)) {{
      return false;
    }}
    {GetNativeArraySerializationCodeInner(fieldInfo)}";
  }

  private static string GetNativeArraySerializationCodeInner(FieldInfo fieldInfo) {
    var subType = fieldInfo.FieldType.GetGenericArguments()[0];
    if (subType == typeof(int) ||
        subType == typeof(float) ||
        subType == typeof(float2) ||
        subType == typeof(float3)) {
      var atts = fieldInfo.GetCustomAttributes(typeof(FieldQuantizationAttribute)).ToList();
      if (atts.Count != 1) {
        throw new ArgumentException("Int fields must be explicitly quantized.");
      }

      var att = atts[0] as FieldQuantizationAttribute;
      return $@"
    foreach (var entry in msg.{fieldInfo.Name}) {{
      if (!Quantization.Quantize(entry, buffer, ref bitOffset, bufferMask, {att.bitsStr}, {att.minStr}, {att.maxStr})) {{
        return false;
      }}
    }}";
    } else if (subType == typeof(bool)) {
      return $@"
    foreach (var entry in msg.{fieldInfo.Name}) {{
      int entryInt = entry ? 1 : 0;
      if (!Quantization.Quantize(entryInt, buffer, ref bitOffset, bufferMask, 1, 0, 2)) {{
        return false;
      }}
    }}";
    } else if (subType.IsEnum) {
      var enumMin = Enum.GetValues(subType).Cast<int>().Min();
      var enumMax = Enum.GetValues(subType).Cast<int>().Max() + 1;
      return $@"
    int __{fieldInfo.Name}Max = (int){subType.FullName}.Count;
    foreach (var entry in msg.{fieldInfo.Name}) {{
      int entryInt = (int)entry;
      if (!Quantization.Quantize(entryInt, buffer, ref bitOffset, bufferMask, -1, {enumMin}, {enumMax})) {{
        return false;
      }}
    }}";
    } else if (HasFieldSerializationAttribute(subType)) {
      return $@"
    foreach (var entry in msg.{fieldInfo.Name}) {{
      if (!Serialize(entry, buffer, ref bitOffset, bufferMask)) {{
        return false;
      }}
    }}";
    }

    throw new ArgumentException($"Type not supported: {subType}");
  }

  private static bool HasFieldSerializationAttribute(Type subType) {
    return subType.GetCustomAttributes(
                    typeof(GenerateMessageFieldSerializationAttribute), true)
                  .Length > 0;
  }

  private static string GetNativeArrayDeserializationCode(FieldInfo fieldInfo) {
    var subType = fieldInfo.FieldType.GetGenericArguments()[0];
    return $@"
    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, 16, 0, 1 << 16, out var {fieldInfo.Name}Length)) {{
      return false;
    }}
    msg.{fieldInfo.Name} = new NativeArray<{subType.FullName}>({fieldInfo.Name}Length, allocator);

    {GetNativeArrayDeserializationCodeInner(fieldInfo, $"{fieldInfo.Name}Length")}";
  }

  private static string GetNativeArrayDeserializationCodeInner(
    FieldInfo fieldInfo,
    string lengthName) {
    var subType = fieldInfo.FieldType.GetGenericArguments()[0];
    if (subType == typeof(int) ||
        subType == typeof(float) ||
        subType == typeof(float2) ||
        subType == typeof(float3)) {
      var atts = fieldInfo.GetCustomAttributes(typeof(FieldQuantizationAttribute)).ToList();
      if (atts.Count != 1) {
        throw new ArgumentException("Int fields must be explicitly quantized.");
      }

      var att = atts[0] as FieldQuantizationAttribute;
      return $@"
    for (int i = 0; i < {lengthName}; i++) {{
      if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, {att.bitsStr}, {att.minStr}, {att.maxStr}, out {subType.FullName} entry)) {{
        return false;
      }}
      msg.{fieldInfo.Name}[i] = entry;
    }}";
    } else if (subType == typeof(bool)) {
      return $@"
    for (int i = 0; i < {lengthName}; i++) {{
      if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, 1, 0, 2, out int entry)) {{
        return false;
      }}
      msg.{fieldInfo.Name}[i] = entry != 0;
    }}";
    } else if (subType.IsEnum) {
      var enumMin = Enum.GetValues(subType).Cast<int>().Min();
      var enumMax = Enum.GetValues(subType).Cast<int>().Max() + 1;
      
      return $@"
    for (int i = 0; i < {lengthName}; i++) {{
      if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, -1, {enumMin}, {enumMax}, out int entry)) {{
        return false;
      }}
      msg.{fieldInfo.Name}[i] = ({subType.FullName})entry;
    }}";
    } else if (subType.GetCustomAttributes(
                        typeof(GenerateMessageFieldSerializationAttribute), true)
                      .Length > 0) {
      return $@"
    for (int i = 0; i < {lengthName}; i++) {{
      if (!Deserialize(buffer, ref bitOffset, allocator, bufferMask, out {subType.FullName} entry)) {{
        return false;
      }}
      msg.{fieldInfo.Name}[i] = entry;
    }}";
    }

    throw new ArgumentException($"Type not supported: {subType}");
  }

  private static string GetEnumSerializationCode(FieldInfo fieldInfo) {
    var enumMin = Enum.GetValues(fieldInfo.FieldType).Cast<int>().Min();
    var enumMax = Enum.GetValues(fieldInfo.FieldType).Cast<int>().Max() + 1;

    return $@"
    int __{fieldInfo.Name}Int = (int)msg.{fieldInfo.Name};
    if (!Quantization.Quantize(__{fieldInfo.Name}Int, buffer, ref bitOffset, bufferMask, -1, {enumMin}, {enumMax})) {{
      return false;
    }}";
  }

  private static string GetEnumDeserializationCode(FieldInfo fieldInfo) {
    var enumMin = Enum.GetValues(fieldInfo.FieldType).Cast<int>().Min();
    var enumMax = Enum.GetValues(fieldInfo.FieldType).Cast<int>().Max() + 1;

    return $@"
    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, -1, {enumMin}, {enumMax}, out int __{fieldInfo.Name}Int)) {{
      return false;
    }}

    msg.{fieldInfo.Name} = ({fieldInfo.FieldType.FullName})__{fieldInfo.Name}Int;";
  }

  private static string GetBoolSerializationCode(FieldInfo fieldInfo) {
    return $@"
    int __{fieldInfo.Name}Int = msg.{fieldInfo.Name} ? 1 : 0;
    if (!Quantization.Quantize(__{fieldInfo.Name}Int, buffer, ref bitOffset, bufferMask, 1, 0, 2)) {{
      return false;
    }}";
  }

  private static string GetBoolDeserializationCode(FieldInfo fieldInfo) {
    return $@"
    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, 1, 0, 2, out int {fieldInfo.Name}Value)) {{
      return false;
    }}
    msg.{fieldInfo.Name} = {fieldInfo.Name}Value != 0;";
  }

  private static string GetQuantizedSerializationCode(FieldInfo fieldInfo) {
    var atts = fieldInfo.GetCustomAttributes(typeof(FieldQuantizationAttribute)).ToList();
    if (atts.Count != 1) {
      throw new ArgumentException($"Field {fieldInfo.DeclaringType}.{fieldInfo.Name} must be explicitly quantized.");
    }

    var att = atts[0] as FieldQuantizationAttribute;
    return $@"
    if (!Quantization.Quantize(msg.{fieldInfo.Name}, buffer, ref bitOffset, bufferMask, {att.bitsStr}, {att.minStr}, {att.maxStr})) {{
      return false;
    }}";
  }
  
  private static string GetQuaternionSerializationCode(FieldInfo fieldInfo) {
    return $@"
    if (!Quantization.QuantizeQuaternion(msg.{fieldInfo.Name}, buffer, ref bitOffset, bufferMask)) {{
      return false;
    }}";
  }

  private static string GetQuantizedDeserializationCode(FieldInfo fieldInfo) {
    var atts = fieldInfo.GetCustomAttributes(typeof(FieldQuantizationAttribute)).ToList();
    if (atts.Count != 1) {
      throw new ArgumentException($"Field {fieldInfo.DeclaringType}.{fieldInfo.Name} must be explicitly quantized.");
    }

    var att = atts[0] as FieldQuantizationAttribute;
    return $@"
    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, {att.bitsStr}, {att.minStr}, {att.maxStr}, out {fieldInfo.FieldType.FullName} {fieldInfo.Name}Value)) {{
      return false;
    }}
    msg.{fieldInfo.Name} = {fieldInfo.Name}Value;";
  }

  private static string GetQuaternionDeserializationCode(FieldInfo fieldInfo) {
    return $@"
    if (!Quantization.DequantizeQuaternion(buffer, ref bitOffset, bufferMask, out {fieldInfo.FieldType.FullName} {fieldInfo.Name}Value)) {{
      return false;
    }}
    msg.{fieldInfo.Name} = {fieldInfo.Name}Value;";
  }
}

#endif
