﻿using System;
using Unity.Mathematics;

[GenerateMessageFieldSerialization]
public struct NetcodeObjectInitializer : IDisposable {
  [FieldQuantization(QuantizationConstants.MinNetObjId, QuantizationConstants.MaxNetObjId)]
  public int id;
  [FieldQuantization(QuantizationConstants.MinPlayerId, QuantizationConstants.MaxPlayerId)]
  public int playerId;
  [FieldQuantization(QuantizationConstants.MinPrefabIndex, QuantizationConstants.MaxPrefabIndex)]
  public int prefabIndex;
  [FieldQuantization("TrackedBodyState.RelPositionMin", "TrackedBodyState.RelPositionMax", "TrackedBodyState.RelPositionBits")]
  public float2 position;
  [FieldQuantization("TrackedBodyState.RotationMin", "TrackedBodyState.RotationMax", "TrackedBodyState.RotationBits")]
  public float rotation;
  [FieldQuantization("TrackedBodyState.VelocityMin", "TrackedBodyState.VelocityMax", "TrackedBodyState.VelocityBits")]
  public float2 linearVelocity;
  [FieldQuantization("TrackedBodyState.AngularVelocityMin", "TrackedBodyState.AngularVelocityMax", "TrackedBodyState.AngularVelocityBits")]
  public float angularVelocity;
  [FieldQuantization(QuantizationConstants.MinServerTick, QuantizationConstants.MaxServerTick)]
  public int expiresOnTick;
  public bool isDeathPrefab;
  [FieldQuantization(0, 32)]
  public int colorIndex;
  public void Dispose() {}
}
