﻿using Unity.Collections;
using Unity.Entities;

[GenerateMessageSerialization]
public struct ServerTickMessage : INetcodeMessage<ServerTickMessage> {
  [FieldQuantization(QuantizationConstants.MinServerTick, QuantizationConstants.MaxServerTick)]
  public int serverTick;

  public bool Process(World world, int sendingPlayerId, bool isServer, ref bool skipDispose) {
    if (isServer) {
      return false;
    }
    
    GameManager.instance.ProcessServerTick(serverTick);
    return true;
  }

  public bool Serialize(NativeArray<byte> buffer, ref int offset, int bufferMask) {
    return NetcodeMessageSerialization.Serialize(this, buffer, ref offset, bufferMask);
  }

  public bool IsReliable() {
    return false;
  }

  public void Dispose() {
  }
}
