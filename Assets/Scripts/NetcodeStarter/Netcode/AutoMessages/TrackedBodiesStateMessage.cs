﻿using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

[GenerateMessageSerialization]
public struct TrackedBodiesStateMessage : INetcodeMessage<TrackedBodiesStateMessage> {
  [FieldQuantization(QuantizationConstants.MinServerTick, QuantizationConstants.MaxServerTick)]
  public int serverTick;

  public NativeArray<TrackedBodyState> trackedBodies;

  public bool Process(World world, int sendingPlayerId, bool isServer, ref bool skipDispose) {
    if (isServer) {
      return false;
    }

    world.GetExistingSystem<GameSystem>().PurgeTempEntities();
    world.GetExistingSystem<GameSystem>()
         .ProcessTrackedBodiesStateMessage(this, ref skipDispose);
    return true;
  }

  public bool Serialize(NativeArray<byte> buffer, ref int offset, int bufferMask) {
    return NetcodeMessageSerialization.Serialize(this, buffer, ref offset, bufferMask);
  }

  public bool IsReliable() {
    return false;
  }

  public void Dispose() {
    trackedBodies.Dispose();
  }
}

[GenerateMessageFieldSerialization]
public struct TrackedBodyState {
  public static readonly int IdBits = 20;
  public static readonly int2 RelPositionBits = new int2(30, 30);
  public static readonly double2 RelPositionMin = new double2(-2500, -2500);
  public static readonly double2 RelPositionMax = new double2(2500, 2500);
  public static readonly int2 VelocityBits = new int2(16, 16);
  public static readonly double2 VelocityMin = new double2(-100, -100);
  public static readonly double2 VelocityMax = new double2(100, 100);
  public static readonly int AngularVelocityBits = 16;
  public static readonly double AngularVelocityMin = -50;
  public static readonly double AngularVelocityMax = 50;
  public static readonly double RotationMin = -2.0 * math.PI_DBL;
  public static readonly double RotationMax = 2.0 * math.PI_DBL;
  public static readonly int RotationBits = 16;

  private static readonly int TotalQuantizedBytes =
    (IdBits +
     RelPositionBits.x + RelPositionBits.y +
     VelocityBits.x + VelocityBits.y +
     AngularVelocityBits +
     RotationBits) / 8 + 1;

  private static readonly NativeArray<byte> TmpQuantizationBytes =
    new NativeArray<byte>(TotalQuantizedBytes, Allocator.Persistent);

  [FieldQuantization(QuantizationConstants.MinNetObjId, QuantizationConstants.MaxNetObjId)]
  public int id;

  [FieldQuantization("TrackedBodyState.RelPositionMin", "TrackedBodyState.RelPositionMax",
                     "TrackedBodyState.RelPositionBits")]
  public float2 position;

  [FieldQuantization("TrackedBodyState.VelocityMin", "TrackedBodyState.VelocityMax",
                     "TrackedBodyState.VelocityBits")]
  public float2 velocity;

  [FieldQuantization("TrackedBodyState.AngularVelocityMin", "TrackedBodyState.AngularVelocityMax",
                     "TrackedBodyState.AngularVelocityBits")]
  public float angularVelocity;

  [FieldQuantization("TrackedBodyState.RotationMin", "TrackedBodyState.RotationMax",
                     "TrackedBodyState.RotationBits")]
  public float rotation;

  public TrackedBodyState ApplyQuantization() {
    var bitOffset = 0;
    if (!NetcodeMessageSerialization.Serialize(this, TmpQuantizationBytes, ref bitOffset, 0xfffffff)
    ) {
      Debug.LogError("Failed to quantize into tmp bytes");
    }

    bitOffset = 0;
    if (!NetcodeMessageSerialization.Deserialize(TmpQuantizationBytes, ref bitOffset,
                                                 Allocator.None, 0xfffffff,
                                                 out TrackedBodyState result)) {
      Debug.LogError("Failed to quantize into tmp bytes");
    }

    return result;
  }
}