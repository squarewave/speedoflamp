﻿using Unity.Collections;
using Unity.Entities;

[GenerateMessageFieldSerialization]
public struct MiniInputState {
  public InputCommand command;
  [FieldQuantization("QuantizationConstants.InputPayloadMin", "QuantizationConstants.InputPayloadMax",
                     "QuantizationConstants.InputPayloadBits")]
  public float aimDirection;
}

[GenerateMessageSerialization]
public struct BulkInputMessage : INetcodeMessage<BulkInputMessage> {
  [FieldQuantization(QuantizationConstants.MinServerTick, QuantizationConstants.MaxServerTick)]
  public int serverTick;
  
  public NativeArray<MiniInputState> inputs;
  
  public void Dispose() {
    inputs.Dispose();
  }

  public bool IsReliable() {
    return false;
  }

  public bool Process(World world, int sendingPlayerId, bool isServer, ref bool skipDispose) {
    if (isServer || sendingPlayerId != NetcodeCommon.ServerPlayerId) {
      return false;
    }

    return world.GetExistingSystem<GameInputSystem>().QueueMiniInputs(serverTick, inputs);
  }

  public bool Serialize(NativeArray<byte> buffer, ref int offset, int bufferMask) {
    return NetcodeMessageSerialization.Serialize(this, buffer, ref offset, bufferMask);
  }
}
