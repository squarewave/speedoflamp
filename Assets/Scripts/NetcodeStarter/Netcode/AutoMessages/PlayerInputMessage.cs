﻿using Unity.Collections;
using Unity.Entities;

[GenerateMessageSerialization]
public struct PlayerInputMessage : INetcodeMessage<PlayerInputMessage> {
  public PlayerInput playerInput;
  
  public void Dispose() {
  }

  public bool IsReliable() {
    return false;
  }

  public bool Process(World world, int sendingPlayerId, bool isServer, ref bool skipDispose) {
    world.GetExistingSystem<GameInputSystem>().QueueInput(playerInput);
    return true;
  }

  public bool Serialize(NativeArray<byte> buffer, ref int offset, int bufferMask) {
    return NetcodeMessageSerialization.Serialize(this, buffer, ref offset, bufferMask);
  }
}
