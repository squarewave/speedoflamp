﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using AOT;
using Unity.Entities;
using UnityEngine;

public class IceProtocolManager : MonoBehaviour {
  public string stunServerHost = "stun.stunprotocol.org";
  public ushort stunServerPort = 3478;

  public static IceProtocolManager instance;
  public event Action<int> OnPeerConnected;
  public event Action<int> OnError;
  
  private static readonly LibJuice.juice_cb_state_changed_t StateChanged = IceStateChanged;
  private static readonly LibJuice.juice_cb_candidate_t Candidate = IceCandidate;
  private static readonly LibJuice.juice_cb_gathering_done_t GatheringDone = IceGatheringDone;
  private static readonly LibJuice.juice_cb_recv_t Recv = IceRecv;
  private readonly Dictionary<int, int> _peerIdToIndex = new Dictionary<int, int>();
  private readonly List<IntPtr> _iceAgents = new List<IntPtr>();
  private readonly List<byte[]> _localDescs = new List<byte[]>();
  private readonly List<IcePeerData> _icePeers = new List<IcePeerData>();
  private int _lastPeerId = 0;
  private List<Action> _mainThreadActionQueue = new List<Action>();
  private Thread _mainThread;
  private GameSystem _gameSystem;

  private void Start() {
    instance = this;
    _mainThread = Thread.CurrentThread;
    _gameSystem = World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<GameSystem>();
  }

  private void OnDestroy() {
    foreach (var agent in _iceAgents) {
      LibJuice.juice_destroy(agent);
    }
  }

  public void InitializeAgentPool(int numAgents) {
    for (int i = 0; i < numAgents; i++) {
      var agent = CreateAgent();
      var sdpBuf = Marshal.AllocHGlobal(LibJuice.JUICE_MAX_SDP_STRING_LEN);
      try {
        LibJuice.juice_get_local_description(agent, sdpBuf, LibJuice.JUICE_MAX_SDP_STRING_LEN);
        _localDescs.Add(Encoding.ASCII.GetBytes(Marshal.PtrToStringAnsi(sdpBuf) + '\0'));
      } finally {
        Marshal.FreeHGlobal(sdpBuf);
      }
    }
  }

  private IntPtr CreateAgent() {
    var turnserv = new LibJuice.juice_turn_server_t {
      host = "turn.spellflingers.com",
      username = "test",
      password = "lizardtonsilsdonatebugs",
      port = 3478,
    };
    var turnservPtr = Marshal.AllocHGlobal(Marshal.SizeOf(turnserv));
    try {
      Marshal.StructureToPtr(turnserv, turnservPtr, false);
      var conf = new LibJuice.juice_config_t {
        stun_server_host = stunServerHost,
        stun_server_port = stunServerPort,
        cb_candidate = Marshal.GetFunctionPointerForDelegate(Candidate),
        cb_gathering_done = Marshal.GetFunctionPointerForDelegate(GatheringDone),
        cb_recv = Marshal.GetFunctionPointerForDelegate(Recv),
        cb_state_changed = Marshal.GetFunctionPointerForDelegate(StateChanged),
        turn_servers = turnservPtr,
        turn_servers_count = 1,
        user_ptr = (IntPtr)_iceAgents.Count,
      };
      var iceAgent = LibJuice.juice_create(ref conf);
      _iceAgents.Add(iceAgent);
      return iceAgent;
    } finally {
      Marshal.FreeHGlobal(turnservPtr);
    }
  }

  [MonoPInvokeCallback(typeof(LibJuice.juice_cb_state_changed_t))]
  private static void IceStateChanged(IntPtr agent, LibJuice.juice_state_t state, IntPtr user_ptr) {
    switch (state) {
      case LibJuice.juice_state_t.JUICE_STATE_FAILED: {
        RunOnMainThread(() => {
          var peer = instance._icePeers[(int) user_ptr];
          instance.OnError?.Invoke(peer.remotePeerId);
        });
        break;
      }
      case LibJuice.juice_state_t.JUICE_STATE_CONNECTED: {
        RunOnMainThread(() => {
          var localAddress = Marshal.AllocHGlobal(256);
          var remoteAddress = Marshal.AllocHGlobal(256);
          LibJuice.juice_get_selected_addresses(agent, localAddress, 256, remoteAddress, 256);
          Debug.Log($"Connected - us: {Marshal.PtrToStringAnsi(localAddress)} them: {Marshal.PtrToStringAnsi(remoteAddress)}");
          
          Marshal.FreeHGlobal(localAddress);
          Marshal.FreeHGlobal(remoteAddress);
          
          var peer = instance._icePeers[(int) user_ptr];
          instance.OnPeerConnected?.Invoke(peer.remotePeerId);
        });
        break;
      }
    }
  }

  [MonoPInvokeCallback(typeof(LibJuice.juice_cb_candidate_t))]
  private static void IceCandidate(IntPtr agent, IntPtr sdp, IntPtr user_ptr) {
    var str = Marshal.PtrToStringAnsi(sdp);
    if (str.Contains("192.168")) {
      return;
    }
    var bytes = Encoding.ASCII.GetBytes(str + '\0');
    RunOnMainThread(() => {
      Debug.Log(str);
      var peer = instance._icePeers[(int) user_ptr];
      peer.candidates.Add(bytes);
    });
  }

  [MonoPInvokeCallback(typeof(LibJuice.juice_cb_gathering_done_t))]
  private static void IceGatheringDone(IntPtr agent, IntPtr user_ptr) {
    RunOnMainThread(() => {
      var peer = instance._icePeers[(int) user_ptr];
      if (peer.gatheringDoneCallback != null) {
        peer.gatheringDoneCallback((int) user_ptr, peer);
        peer.gatheringDoneCallback = null;
      }
    });
  }

  [MonoPInvokeCallback(typeof(LibJuice.juice_cb_recv_t))]
  private static void IceRecv(IntPtr agent, IntPtr data, ulong size, IntPtr userPtr) {
    instance._gameSystem.common.ReceiveCallback(agent, data, (int) size);
  }

  private static void RunOnMainThread(Action a) {
    if (Thread.CurrentThread == instance._mainThread) {
      a();
    } else {
      lock (instance._mainThreadActionQueue) {
        instance._mainThreadActionQueue.Add(a);
      }
    }
  }

  public void BeginAddPeer(byte[] remoteDesc, int peerId, Action<int, byte[][]> candidatesReady, Action error) {
    _peerIdToIndex.Add(peerId, _icePeers.Count);
    if (_icePeers.Count == _iceAgents.Count) {
      throw new InvalidOperationException();
    }

    var agent = _iceAgents[_icePeers.Count];
    var peer = new IcePeerData {
      playerId = ++_lastPeerId,
      remotePeerId = peerId,
      agent = agent,
      gatheringDoneCallback = (agentIndex, p) => {
        candidatesReady(p.remotePeerId, p.candidates.ToArray());
      },
    };
    _icePeers.Add(peer);

    var buf = Marshal.AllocHGlobal(remoteDesc.Length);
    Marshal.Copy(remoteDesc, 0, buf, remoteDesc.Length);
    LibJuice.juice_set_remote_description(agent, buf);
    LibJuice.juice_gather_candidates(agent);
    Marshal.FreeHGlobal(buf);
  }

  private void Update() {
    lock (_mainThreadActionQueue) {
      foreach (var action in _mainThreadActionQueue) {
        try {
          action();
        } catch (Exception e) {
          if (e is OutOfMemoryException || e is StackOverflowException) {
            throw;
          }
          Debug.Log(e.ToString());
        }
      }
      
      _mainThreadActionQueue.Clear();
    }
  }

  public void SetRemoteCandidates(int peerId, byte[][] remoteCandidates) {
    var agentIndex = _peerIdToIndex[peerId];
    var agent = _iceAgents[agentIndex];
    foreach (var candidate in remoteCandidates) {
      var buf = Marshal.AllocHGlobal(candidate.Length);
      Marshal.Copy(candidate, 0, buf, candidate.Length);
      LibJuice.juice_add_remote_candidate(agent, buf);
      Marshal.FreeHGlobal(buf);
    }
    LibJuice.juice_set_remote_gathering_done(agent);
  }

  public static void Send(IntPtr agent, byte[] buffer, int offset) {
    var buf = Marshal.AllocHGlobal(buffer.Length);
    Marshal.Copy(buffer, 0, buf, buffer.Length);
    LibJuice.juice_send(agent, buf, (ulong)offset);
    Marshal.FreeHGlobal(buf);
  }

  public void StartClient(int peerId) {
    var agentIndex = _peerIdToIndex[peerId];
    _gameSystem.StartClient(_iceAgents[agentIndex]);
  }
  
  public void StartServer() {
    _gameSystem.StartServer();
  }

  public byte[] GetLocalDesc(int peerIndex) {
    return _localDescs[peerIndex];
  }
}

internal class IcePeerData {
  public int playerId;
  public int remotePeerId;
  public IntPtr agent;
  public Action<int, IcePeerData> gatheringDoneCallback;
  public readonly List<byte[]> candidates = new List<byte[]>();
}
