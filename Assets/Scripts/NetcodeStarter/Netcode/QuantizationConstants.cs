﻿using Unity.Mathematics;

public static class QuantizationConstants {
  public const long MinPlayerId = -1;
  public const long MaxPlayerId = 62;
  public const long MinNetObjId = 0;
  public const long MaxNetObjId = 1 << 20;
  public const long MinPrefabIndex = 0;
  public const long MaxPrefabIndex = 1 << 9;
  public const long MinServerTick = -256;
  public const long MaxServerTick = (long)int.MaxValue + 1;
  public static readonly int InputPayloadBits = 7;
  public static readonly double InputPayloadMin = -math.PI_DBL;
  public static readonly double InputPayloadMax = math.PI_DBL;
}
