﻿using System;
using System.Collections.Generic;
using System.Linq;
using Unity.Collections;
using UnityEngine;

public static class ServerTickStatic {
  public static int serverTick;
}

public enum ServerTickSelectionCriteria {
  ExactlyAtTick,
  AtOrPastTick,
}

public class ServerTickedMessageQueue<T> where T : IDisposable {
  private struct ServerTickedMessage {
    public T message;
    public int serverTick;
  }
  private readonly List<ServerTickedMessage> _messages = new List<ServerTickedMessage>();
  private readonly ServerTickSelectionCriteria _selectionCritera;

  public ServerTickedMessageQueue(ServerTickSelectionCriteria criteria) {
    _selectionCritera = criteria;
  }

  public List<T> GetMessagesForTick(int serverTick) {
    return _messages.Where(m => m.serverTick == serverTick).Select(m => m.message).ToList();
  }
  
  public void QueueMessage(int serverTick, T message) {
    var tickedMessage = new ServerTickedMessage {
      message = message,
      serverTick = serverTick,
    };
    for (int i = 0; i < _messages.Count; i++) {
      if (_messages[i].serverTick < serverTick) {
        _messages.Insert(i, tickedMessage);
        return;
      }
    }
    _messages.Add(tickedMessage);
  }

  public bool IsEmpty() {
    for (int i = _messages.Count - 1; i >= 0; i--) {
      if (_messages[i].serverTick > ServerTickStatic.serverTick) {
        return false;
      }

      if (_messages[i].serverTick < ServerTickStatic.serverTick &&
          _selectionCritera == ServerTickSelectionCriteria.ExactlyAtTick) {
        _messages[i].message.Dispose();
        _messages.RemoveAt(i);
        continue; 
      }

      return false;
    }
    return true;
  }

  public bool GetNextMessage(out T message) {
    message = default;

    for (int i = _messages.Count - 1; i >= 0; i--) {
      if (_messages[i].serverTick > ServerTickStatic.serverTick) {
        return false;
      }

      if (_messages[i].serverTick < ServerTickStatic.serverTick &&
          _selectionCritera == ServerTickSelectionCriteria.ExactlyAtTick) {
        Debug.Log($"Removing {typeof(T).Name} (msg: {_messages[i].serverTick}, ours: {ServerTickStatic.serverTick})");
        _messages[i].message.Dispose();
        _messages.RemoveAt(i);
        continue; 
      }

      if (_messages[i].serverTick < ServerTickStatic.serverTick) {
        Debug.Log($"Outdated server tick for {typeof(T).Name} (msg: {_messages[i].serverTick}, ours: {ServerTickStatic.serverTick})");
      }
      message = _messages[i].message;
      _messages.RemoveAt(i);
      return true;
    }
    return false;
  }

  public void Clear() {
    foreach (var message in _messages) {
      message.message.Dispose();
    }
    
    _messages.Clear();
  }
}
