﻿using System.Collections.Generic;
using System.Threading;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;
using UnityEngine.SceneManagement;

[UpdateInGroup(typeof(PrePhysicsGroup))]
[AlwaysUpdateSystem]
public class GameInputSystem : ComponentSystem {
  public int playerId;

  private readonly Dictionary<int, int> _unitsToPlayers =
    new Dictionary<int, int>();

  private int _latestPlayerId;
  private GameSystem _game;

  private readonly ServerTickedMessageQueue<PlayerInput> _playerInputQueue =
    new ServerTickedMessageQueue<PlayerInput>(ServerTickSelectionCriteria.AtOrPastTick);

  protected override void OnCreate() {
    SceneManager.sceneUnloaded += SceneManagerOnsceneUnloaded;
    
    InitEntityQueryCache(30);
    _game = World.GetOrCreateSystem<GameSystem>();
  }

  private void SceneManagerOnsceneUnloaded(Scene s) {
    playerId = 0;
    _latestPlayerId = 0;
    _unitsToPlayers.Clear();
    _playerInputQueue.Clear();
  }

  public List<PlayerInput> GetInputsForTick(int serverTick) {
    return _playerInputQueue.GetMessagesForTick(serverTick);
  }

  public int GetPlayerForUnit(int unitId) {
    if (_unitsToPlayers.TryGetValue(unitId, out int playerId)) {
      return playerId;
    }

    Debug.LogError($"Player does not exist for unit {unitId}");
    return int.MinValue;
  }
  
  
  public void QueueInput(PlayerInput playerInput, bool bypassRebroadcast = false) {
    if (!_game.started) {
      return;
    }
    if (!bypassRebroadcast && playerInput.playerId == playerId && !_game.IsServer()) {
      _game.QueueMessageOrBroadcast(new PlayerInputMessage {
        playerInput = playerInput
      });
    }

    _playerInputQueue.QueueMessage(playerInput.serverTick, playerInput);
  }

  public void UnqueueInputs(NativeList<PlayerInput> inputs) {
    while (_playerInputQueue.GetNextMessage(out var input)) {
      inputs.Add(input);
    }
  }

  public int GetNewPlayerId() {
    if (!_game.IsServer()) {
      Debug.LogError("Client requesting a new player id.");
      return -1;
    }

    return Interlocked.Increment(ref _latestPlayerId);
  }

  public void RegisterUnit(int unitId, int playerId) {
    _unitsToPlayers[unitId] = playerId;
  }

  protected override void OnUpdate() {
  }

  public bool QueueMiniInputs(int serverTick, NativeArray<MiniInputState> inputs) {
    for (int i = 0; i < inputs.Length; i++) {
      QueueInput(new PlayerInput {
        command = inputs[i].command,
        payload = inputs[i].aimDirection,
        playerId = i + 1,
        serverTick = serverTick,
      }, true);
    }

    return true;
  }

  public int NumPlayers() {
    return _latestPlayerId;
  }
}

public enum InputCommand {
  shootStop = 0,
  shoot,
}
