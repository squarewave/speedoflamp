﻿using System;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

[UpdateInGroup(typeof(PrePhysicsGroup))]
[UpdateBefore(typeof(GameInputSystem))]
[UpdateBefore(typeof(PlayerInputSystem))]
[AlwaysUpdateSystem]
public class PrePhysicsGameSystem : SystemBase {
  private readonly ServerTickedMessageQueue<ObjectInstantiatedMessage> _deferredInitializations =
    new ServerTickedMessageQueue<ObjectInstantiatedMessage>(
      ServerTickSelectionCriteria.AtOrPastTick);

  private GameSystem _game;
  private GameInputSystem _input;

  protected override void OnCreate() {
    SceneManager.sceneUnloaded += SceneManagerOnsceneUnloaded;
    _game = World.GetOrCreateSystem<GameSystem>();
    _input = World.GetOrCreateSystem<GameInputSystem>();
    base.OnCreate();
  }

  private void SceneManagerOnsceneUnloaded(Scene arg0) {
    _deferredInitializations.Clear();
  }

  protected override void OnUpdate() {
    while (_deferredInitializations.GetNextMessage(out var message)) {
      _game.InstantiateObject(message.initializer, message.childIds);
    }

    if (_game.IsServer()) {
      var inputs = _input.GetInputsForTick(ServerTickStatic.serverTick);
      var miniInputs = new NativeArray<MiniInputState>(_input.NumPlayers(), Allocator.Temp);
      foreach (var input in inputs) {
        miniInputs[input.playerId - 1] = new MiniInputState {
          command = input.command,
          aimDirection = input.payload,
        };
      }
    
      _game.server.QueueBroadcast(new BulkInputMessage {
        inputs = miniInputs,
        serverTick = ServerTickStatic.serverTick,
      }, true);
    }
  }

  public void DeferInstantiateObject(ObjectInstantiatedMessage message) {
    _deferredInitializations.QueueMessage(message.serverTick, message);
  }
}

[UpdateInGroup(typeof(PostPhysicsGroup))]
public class GameSystem : SystemBase {
  private int _ticksPerStateSync = 4;
  private int _jitterBufferTicks = 6;

  public readonly Dictionary<int, float> playerPings = new Dictionary<int, float>();

  private int _mostRecentInstantiatedObjectId;
  private int _mostRecentInstantiatedTempObjectId;
  private int _trackedBodyIndex;

  public NetcodeClient client;
  public NetcodeServer server;
  public NetcodeCommon common;
  public bool started;

  private readonly ServerTickedMessageQueue<TrackedBodiesStateMessage> _remoteServerState =
    new ServerTickedMessageQueue<TrackedBodiesStateMessage>(
      ServerTickSelectionCriteria.ExactlyAtTick);

  private const int MaxTrackedBodies = 10;
  private const float LinearVelocitySyncScaling = 0.1f;
  private const float AngularVelocitySyncScaling = 0.1f;
  private const float JitterBufferTicksFloatApproach = .33f;

  public int _currentClientTick;

  private float _jitterBufferTicksFloat = 6;
  private float _lastPingReceived;
  private float _lastPingSent;
  private float _lastPingRoundtrip;
  private bool _isConnected;
  private GameInputSystem _input;
  private EntityQuery _snapshotPhysicsQuery;
  private EntityQuery _temporaryObjectsQuery;
  private EntityQuery _netcodeObjectsQuery;

  private const float PhysicsSyncSmoothingThresholdSq = 1f;

  protected override void OnCreate() {
    SceneManager.sceneUnloaded += SceneManagerOnsceneUnloaded;
    _input = World.GetOrCreateSystem<GameInputSystem>();
    _temporaryObjectsQuery = GetEntityQuery(typeof(TemporaryEntity));
    _netcodeObjectsQuery = GetEntityQuery(typeof(NetcodeObject));
    _playersQuery = GetEntityQuery(typeof(PlayerData));
  }

  private void SceneManagerOnsceneUnloaded(Scene arg0) {
    _ticksPerStateSync = 4;
    _jitterBufferTicks = 6;
    playerPings.Clear();

    _mostRecentInstantiatedObjectId = 0;
    _mostRecentInstantiatedTempObjectId = 0;
    _trackedBodyIndex = 0;

    common.Dispose();
    common = null;
    server = null;
    client = null;
    started = false;

    _remoteServerState.Clear();
    _currentClientTick = 0;

    _jitterBufferTicksFloat = 6;
    _lastPingReceived = 0;
    _lastPingSent = 0;
    _lastPingRoundtrip = 0;
    _isConnected = false;
    
    EntityManager.DestroyEntity(EntityManager.UniversalQuery);
  }

  protected override void OnUpdate() {
    if (IsClient() && !_isConnected) {
      return;
    }

    _currentClientTick++;

    if (IsClient()) {
      ApplyPendingTrackedBodiesStateMessages();
      if (Time.ElapsedTime - _lastPingReceived > 1f) {
        client.QueueSendToServer(new PingMessage {
          lastPingTime = _lastPingRoundtrip,
          estimatedServerTick = ServerTickStatic.serverTick,
        });
        _lastPingReceived = float.MaxValue;
        _lastPingSent = (float) Time.ElapsedTime;
      }
    } else if (IsServer()) {
      if (_currentClientTick % _ticksPerStateSync == 0) {
        var allocator = Allocator.Temp;

        var serverStateMessage = CollectPhysicsSnapshot(allocator);
        server.QueueBroadcast(serverStateMessage, true);

        for (int i = 0; i < serverStateMessage.trackedBodies.Length; i++) {
          serverStateMessage.trackedBodies[i] = serverStateMessage.trackedBodies[i].ApplyQuantization();
        }

        ApplyPhysicsSnapshot(serverStateMessage);
        serverStateMessage.Dispose();
      }
    }
  }

  private struct SyncableBody : IComparable<SyncableBody> {
    public SyncScore syncScore;
    public int entityInQueryIndex;
    public TrackedBodyState trackedBodyState;

    public int CompareTo(SyncableBody other) {
      return other.syncScore.value.CompareTo(syncScore.value);
    }
  }

  private TrackedBodiesStateMessage CollectPhysicsSnapshot(Allocator allocator) {
    var trackedBodiesCount = _snapshotPhysicsQuery.CalculateEntityCount();
    var trackedBodiesScored = new NativeArray<SyncableBody>(trackedBodiesCount, Allocator.Temp);
    Entities.ForEach((int entityInQueryIndex,
                      ref SyncScore syncScore,
                      in NetcodeObject netObj,
                      in PhysicsVelocity velocity,
                      in Translation translation,
                      in Rotation rotation,
                      in SyncedPhysics syncedPhysics) => {
      syncScore.value += 1f;
      syncScore.value += math.length(velocity.Linear.xy) * LinearVelocitySyncScaling;
      syncScore.value += velocity.Angular.z * AngularVelocitySyncScaling;

      trackedBodiesScored[entityInQueryIndex] = new SyncableBody {
        syncScore = syncScore,
        entityInQueryIndex = entityInQueryIndex,
        trackedBodyState = new TrackedBodyState {
          id = netObj.id,
          position = translation.Value.xy,
          rotation = GetZAxisAngle(rotation.Value),
          velocity = velocity.Linear.xy,
          angularVelocity = velocity.Angular.z,
        }
      };
    }).WithStoreEntityQueryInField(ref _snapshotPhysicsQuery).Run();
    trackedBodiesScored.Sort();
    var itemsToSync = math.min(MaxTrackedBodies, trackedBodiesCount);
    var syncScores = new NativeArray<SyncScore>(trackedBodiesCount, Allocator.Temp);
    var trackedBodies = new NativeArray<TrackedBodyState>(itemsToSync, allocator);
    for (int i = 0; i < itemsToSync; i++) {
      trackedBodies[i] = trackedBodiesScored[i].trackedBodyState;
    }

    for (int i = 0; i < trackedBodiesCount; i++) {
      var syncScore = trackedBodiesScored[i].syncScore;
      if (i < MaxTrackedBodies) {
        syncScore.value = 0f;
      }
      syncScores[trackedBodiesScored[i].entityInQueryIndex] = syncScore;
    }
    
    _snapshotPhysicsQuery.CopyFromComponentDataArray(syncScores);
    var result = new TrackedBodiesStateMessage {
      serverTick = ServerTickStatic.serverTick,
      trackedBodies = trackedBodies
    };
    return result;
  }

  private void ApplyPendingTrackedBodiesStateMessages() {
    while (_remoteServerState.GetNextMessage(out var message)) {
      ApplyPhysicsSnapshot(message);
      message.Dispose();
    }
  }

  private void ApplyPhysicsSnapshot(TrackedBodiesStateMessage message) {
    var idToMsgIndex = new NativeHashMap<int, int>(message.trackedBodies.Length, Allocator.Temp);
    for (int i = 0; i < message.trackedBodies.Length; i++) {
      idToMsgIndex.Add(message.trackedBodies[i].id, i);
    }

    Entities.ForEach((Entity entity,
                      ref PhysicsVelocity velocity,
                      ref Translation translation,
                      ref Rotation rotation,
                      ref SyncTransform sync,
                      in NetcodeObject netObj,
                      in SyncedPhysics _) => {
      if (idToMsgIndex.TryGetValue(netObj.id, out var msgTrackedObjIndex)) {
        var msgTrackedObj = message.trackedBodies[msgTrackedObjIndex];
        float3 msgPosition = new float3(msgTrackedObj.position, 0);
        float3 msgVelocity = new float3(msgTrackedObj.velocity, 0);
        float3 msgAngularVelocity = new float3(0, 0, msgTrackedObj.angularVelocity);
        quaternion msgRotation = GetZAxisQuaternion(msgTrackedObj.rotation);
        velocity = new PhysicsVelocity {Angular = msgAngularVelocity, Linear = msgVelocity};

        float3 clientPosition = translation.Value;
        
        float3 serverPosition = msgPosition;
        if (math.distancesq(clientPosition, serverPosition) <
            PhysicsSyncSmoothingThresholdSq) {
          sync.positionOffset += clientPosition - serverPosition;
          // var rotationOffset = ((Quaternion) rotations[index].Value).eulerAngles -
          //                      ((Quaternion) msgTrackedObj.rotation).eulerAngles;
          // sync.rotationOffset += (float3) rotationOffset;
        } else {
          sync = default;
        }

        translation.Value = serverPosition;
        rotation.Value = msgRotation;
      }
    }).WithReadOnly(idToMsgIndex).WithReadOnly(message.trackedBodies).WithDisposeOnCompletion(idToMsgIndex).Run();
  }

  private static quaternion GetZAxisQuaternion(float angle) {
    var result = quaternion.RotateZ(angle);
    return result;
  }

  private static float GetZAxisAngle(quaternion q) {
    var result = math.atan2(q.value.z, q.value.w) * 2f;
    return result;
  }

  public void QueueMessageOrBroadcast<T>(T message) where T : INetcodeMessage<T> {
    if (IsServer()) {
      server.QueueBroadcast(message, true);
    } else if (IsClient()) {
      client.QueueSendToServer(message);
    }
  }

  public bool IsClient() {
    return client != null;
  }

  public bool IsServer() {
    return server != null;
  }

  public void StartServer() {
    if (server != null) {
      return;
    }

    Debug.Log($"Starting server");
    server = new NetcodeServer(World);
    common = server;
    server.Start();
    started = true;
  }

  private EntityQuery _playersQuery;

  public void StartClient(IntPtr hostPeer) {
    if (client != null) {
      return;
    }

    client = new NetcodeClient(World);
    common = client;
    client.Start(hostPeer);
    started = true;
  }

  public bool InitializeNewPlayer(int playerId, HelloServerMessage message) {
    AddPlayerDataEntity(playerId);

    var netcodeObjQuery =
      GetEntityQuery(typeof(NetcodeObject), typeof(Translation), typeof(Rotation), typeof(PhysicsVelocity));
    var netcodeObjCount = netcodeObjQuery.CalculateEntityCount();
    var netcodeObjectInitializers =
      new NativeArray<NetcodeObjectInitializer>(netcodeObjCount, Allocator.Temp);
    using var entities = netcodeObjQuery.ToEntityArray(Allocator.TempJob);
    using var netcodeObjects = netcodeObjQuery.ToComponentDataArray<NetcodeObject>(Allocator.Temp);
    using var rotations = netcodeObjQuery.ToComponentDataArray<Rotation>(Allocator.Temp);
    using var translations = netcodeObjQuery.ToComponentDataArray<Translation>(Allocator.Temp);
    using var velocities = netcodeObjQuery.ToComponentDataArray<PhysicsVelocity>(Allocator.Temp);
    for (int i = 0; i < netcodeObjCount; i++) {
      var netcodeObj = netcodeObjects[i];
      netcodeObjectInitializers[i] = new NetcodeObjectInitializer {
        id = netcodeObj.id,
        playerId = netcodeObj.playerId,
        prefabIndex = netcodeObj.prefabIndex,
        rotation = GetZAxisAngle(rotations[i].Value),
        position = translations[i].Value.xy,
        linearVelocity = velocities[i].Linear.xy,
        angularVelocity = velocities[i].Angular.z
      };
    }

    var players = _playersQuery.ToComponentDataArray<PlayerData>(Allocator.Temp);
    using var childIds = new NativeList<int>(Allocator.Temp);
    server.QueueClientMessage(playerId, new HelloClientMessage {
      playerId = playerId,
      players = players,
      netcodeObjectInitializers = netcodeObjectInitializers,
      childIds = childIds
    });
    players.Dispose();

    server.QueueBroadcast(new PlayerEnteredMessage {
      playerId = playerId,
    }, true, playerId);

    netcodeObjectInitializers.Dispose();
    return true;
  }

  public int GetNextNetcodeObjectId() {
    return IsServer() ? ++_mostRecentInstantiatedObjectId : --_mostRecentInstantiatedTempObjectId;
  }

  public bool InitializeClient(HelloClientMessage message) {
    var input = _input;
    if (input.playerId != NetcodeObject.UnassignedPlayerId) {
      Debug.LogError("Initializing client twice");
      return false;
    }

    _isConnected = true;
    input.playerId = message.playerId;

    foreach (var playerData in message.players) {
      AddPlayerDataEntity(playerData.id);
    }

    var childIdsIndex = 0;
    foreach (var initializer in message.netcodeObjectInitializers) {
      if (initializer.prefabIndex == NetcodeObject.UnassignedPrefabIndex) {
        continue;
      }

      InstantiatePrefab(initializer.prefabIndex, initializer.playerId, initializer.id,
                        initializer.isDeathPrefab,
                        new float3(initializer.position, 0),
                        GetZAxisQuaternion(initializer.rotation),
                        new float3(initializer.linearVelocity, 0),
                        new float3(0, 0, initializer.angularVelocity),
                        GameManager.instance.PlayerColor(initializer.playerId),
                        initializer.expiresOnTick,
                        ref message.childIds, ref childIdsIndex);
    }

    return true;
  }

  public void ProcessTrackedBodiesStateMessage(TrackedBodiesStateMessage message,
                                               ref bool skipDispose) {
    if (!IsClient()) {
      return;
    }

    Debug.DrawLine(Vector3.zero, Vector3.up + Vector3.forward, Color.black);

    if (message.serverTick < ServerTickStatic.serverTick) {
      return;
    }

    _remoteServerState.QueueMessage(message.serverTick, message);
    skipDispose = true;
  }

  public int GetJitterBufferedServerTick() {
    return ServerTickStatic.serverTick + (IsServer() ? 6 : (_jitterBufferTicks  + 2));
  }

  public Entity InstantiateObject(NetcodeObjectInitializer initializer) {
    var playerId = initializer.playerId;
    var objectId = initializer.id;
    var prefabIndex = initializer.prefabIndex;
    var isDeathPrefab = initializer.isDeathPrefab;
    var translation = initializer.position;
    var rotation = initializer.rotation;
    var linearVelocity = initializer.linearVelocity;
    var angularVelocity = initializer.angularVelocity;
    var color = GameManager.instance.PlayerColor(initializer.playerId);

    var childIdsIndex = 0;
    var childIds = new NativeArray<int>(32, Allocator.TempJob);
    Entity result = InstantiatePrefab(prefabIndex, playerId, objectId, isDeathPrefab,
                                      new float3(translation, 0), GetZAxisQuaternion(rotation),
                                      new float3(linearVelocity, 0),
                                      new float3(0, 0, angularVelocity),
                                      color, initializer.expiresOnTick,
                                      ref childIds, ref childIdsIndex);
    if (IsServer()) {
      if (IsTempObject(objectId)) {
        Debug.LogError("Trying to instantiate a temp object on server");
      }

      if (EntityManager.HasComponent<SyncedPhysics>(result)) {
        server.QueueBroadcast(new ObjectInstantiatedMessage {
          initializer = initializer,
          childIds = childIds,
          serverTick = ServerTickStatic.serverTick,
        }, true);
      }
    } else {
      if (!IsTempObject(objectId)) {
        Debug.LogError("Trying to instantiate a non-temp object on client");
      }

      if (EntityManager.HasComponent<SyncedPhysics>(result)) {
        EntityManager.AddComponentData(result, new TemporaryEntity());
      }
    }

    if (prefabIndex == (int) AllNetcodePrefabs.PlayerLamp) {
      _input.RegisterUnit(objectId, playerId);
    }

    childIds.Dispose();

    return result;
  }

  public void PurgeTempEntities() {
    var tempEntities = _temporaryObjectsQuery.ToEntityArray(Allocator.TempJob);
    foreach (var entity in tempEntities) {
      if (EntityManager.HasComponent<GameObject>(entity)) {
        var obj = EntityManager.GetComponentObject<GameObject>(entity);
        Object.Destroy(obj);
      }

      EntityManager.DestroyEntity(entity);
    }

    tempEntities.Dispose();
  }

  private bool IsTempObject(int objectId) {
    return objectId < 0;
  }

  public Entity InstantiateObject(NetcodeObjectInitializer initializer, NativeArray<int> childIds) {
    var playerId = initializer.playerId;
    var objectId = initializer.id;
    var prefabIndex = initializer.prefabIndex;
    var translation = initializer.position;
    var rotation = initializer.rotation;
    var linearVelocity = initializer.linearVelocity;
    var angularVelocity = initializer.angularVelocity;
    var isDeathPrefab = initializer.isDeathPrefab;
    var color = GameManager.instance.PlayerColor(initializer.playerId);

    var childIdsIndex = 0;
    return InstantiatePrefab(prefabIndex, playerId, objectId, isDeathPrefab,
                             new float3(translation, 0), GetZAxisQuaternion(rotation),
                             new float3(linearVelocity, 0), new float3(0, 0, angularVelocity),
                             color,
                             initializer.expiresOnTick,
                             ref childIds, ref childIdsIndex);
  }

  private Entity InstantiatePrefab(int prefabIndex,
                                   int playerId,
                                   int objectId,
                                   bool isDeathPrefab,
                                   float3 translation,
                                   quaternion rotation,
                                   float3 linearVelocity,
                                   float3 angularVelocity,
                                   Color color,
                                   int expiresOnTick,
                                   ref NativeArray<int> childIds,
                                   ref int childNetcodeIdsIndex) {
    if (prefabIndex == NetcodeObject.UnassignedPrefabIndex) {
      Debug.LogError("Trying to instantiate unassigned prefab");
      return Entity.Null;
    }
    
    var prefabList = isDeathPrefab
      ? GetSingleton<NetcodePrefabList>().deathPrefabs
      : GetSingleton<NetcodePrefabList>().prefabs;
    var prefabObjList = isDeathPrefab
      ? NetcodePrefabListSingleton.instance.deathPrefabs
      : NetcodePrefabListSingleton.instance.prefabs;
    var prefab = EntityManager.GetBuffer<NetcodePrefabBuffer>(prefabList)[prefabIndex];
    var entity = EntityManager.Instantiate(prefab.value);

    // Set up the netcode object - we have to assume that this exists, as its the only way to track
    // instantiated netcode prefabs.
    var netcodeObject = new NetcodeObject {
      playerId = playerId, id = objectId, prefabIndex = prefabIndex, isDeathPrefab = true
    };
    EntityManager.AddComponentData(entity, netcodeObject);
    var syncScore = new SyncScore();
    EntityManager.AddComponentData(entity, syncScore);
    EntityManager.SetComponentData(entity, new Translation {
      Value = translation
    });
    EntityManager.SetComponentData(entity, new Rotation {
      Value = rotation
    });
    EntityManager.SetComponentData(entity, new PhysicsVelocity {
      Linear = linearVelocity,
      Angular = angularVelocity,
    });

    if (expiresOnTick > 0) {
      EntityManager.AddComponentData(entity, new ExpiresOnTick {
        tick = expiresOnTick
      });
    }

    // Netcode objects with SyncTransform also need a corresponding GameObject
    GameObject entityObj = null;
    var syncTransforms = EntityManager.HasComponent<SyncTransform>(entity);
    if (syncTransforms) {
      entityObj = Object.Instantiate(prefabObjList[prefabIndex]);
      if (entityObj.TryGetComponent(out Renderer renderer)) {
        renderer.material.color = color;
      } else {
        entityObj.GetComponentInChildren<Renderer>().material.color = color;
      }
      EntityManager.AddComponentObject(entity, entityObj);
    }

    if (EntityManager.HasComponent<LinkedEntityGroup>(entity)) {
      var childrenBuf = EntityManager.GetBuffer<LinkedEntityGroup>(entity);
      var childrenArray = new NativeArray<Entity>(childrenBuf.Length, Allocator.TempJob);
      for (int i = 0; i < childrenBuf.Length; i++) {
        childrenArray[i] = childrenBuf[i].Value;
      }
    
      var childEntityIndex = 0;
      var childObjIndex = 0;
      for (int i = 1; i < childrenArray.Length; i++) {
        var child = childrenArray[i];
        if (syncTransforms && (!EntityManager.HasComponent<Parent>(child) ||
            EntityManager.GetComponentData<Parent>(child).Value == prefab.value) &&
            EntityManager.HasComponent<SyncTransform>(child)) {
          var gameObject = entityObj.transform.GetChild(childObjIndex++).gameObject;
          EntityManager.AddComponentObject(child, gameObject);
        }

        if (EntityManager.HasComponent<Translation>(child)) {
          EntityManager.SetComponentData(child, new Translation {
            Value = translation
          });
          EntityManager.SetComponentData(child, new Rotation {
            Value = rotation
          }); 
        }
    
        // Only sync physics children, on the assumption that they are the only objects
        // which will meaningfully diverge from their parents and affect the game state.
        if (EntityManager.HasComponent<PhysicsVelocity>(child)) {
          if (IsServer()) {
            var childSyncScore = new SyncScore();
            EntityManager.AddComponentData(child, childSyncScore);
            var childNetObj = new NetcodeObject {
              playerId = playerId,
              id = GetNextNetcodeObjectId(),
              prefabIndex = NetcodeObject.UnassignedPrefabIndex,
              isDeathPrefab = true
            };
            EntityManager.AddComponentData(child, childNetObj);
            childIds[childEntityIndex++] = childNetObj.id;
          } else {
            var childSyncScore = new SyncScore();
            EntityManager.AddComponentData(child, childSyncScore);
            var childNetObj = new NetcodeObject {
              playerId = playerId,
              id = IsTempObject(objectId)
                ? GetNextNetcodeObjectId()
                : childIds[childNetcodeIdsIndex++],
              prefabIndex = NetcodeObject.UnassignedPrefabIndex,
              isDeathPrefab = true
            };
            EntityManager.AddComponentData(child, childNetObj);
    
            if (IsTempObject(objectId)) {
              EntityManager.AddComponentData(child, new TemporaryEntity());
            }
          }
        }
      }
    
      childrenArray.Dispose();
    }

    return entity;
  }

  public void ProcessPingMessage(int playerId, PingMessage message) {
    if (IsClient()) {
      _lastPingRoundtrip = (float) (Time.ElapsedTime - _lastPingSent);
      _lastPingReceived = (float) Time.ElapsedTime;

      var deltaTick = message.estimatedServerTick;
      _jitterBufferTicksFloat += (deltaTick - _jitterBufferTicksFloat) *
                                 JitterBufferTicksFloatApproach;
      _jitterBufferTicks = Mathf.CeilToInt(_jitterBufferTicksFloat) + 1;
    } else if (IsServer()) {
      playerPings[playerId] = message.lastPingTime;
      message.estimatedServerTick = ServerTickStatic.serverTick - message.estimatedServerTick;
      server.QueueClientMessage(playerId, message);
    }
  }

  public void DestroyObjects(NativeArray<int> objectIds) {
    if (objectIds.Length == 0) {
      return;
    }

    var netcodeObjectsCount = _netcodeObjectsQuery.CalculateEntityCount();
    var entities = _netcodeObjectsQuery.ToEntityArray(Allocator.TempJob);
    var netObjects = _netcodeObjectsQuery.ToComponentDataArray<NetcodeObject>(Allocator.TempJob);
    var idToIndex = new NativeHashMap<int, int>(netcodeObjectsCount, Allocator.TempJob);
    for (int i = 0; i < netcodeObjectsCount; i++) {
      idToIndex.Add(netObjects[i].id, i);
    }

    foreach (int id in objectIds) {
      if (idToIndex.ContainsKey(id)) {
        var entity = entities[idToIndex[id]];
        DestroyEntity(entity);
        idToIndex.Remove(id);
      }
    }

    if (IsServer()) {
      server.QueueBroadcast(new ObjectsDestroyedMessage {
        objectIds = objectIds
      }, true);
    }

    netObjects.Dispose();
    idToIndex.Dispose();
    entities.Dispose();
  }

  public void DestroyEntity(Entity entity, int netObjId) {
    DestroyEntity(entity);

    if (IsServer()) {
      var objectIds = new NativeList<int>(Allocator.TempJob) {netObjId};
      server.QueueBroadcast(new ObjectsDestroyedMessage {
        objectIds = objectIds
      }, true);
      objectIds.Dispose();
    }
  }

  private void DestroyEntity(Entity entity) {
    var gameObj = EntityManager.GetComponentObject<GameObject>(entity);
    if (EntityManager.HasComponent<DetachChildrenOnDestroy>(entity)) {
      gameObj.transform.DetachChildren();
    }

    Object.Destroy(gameObj);
    EntityManager.DestroyEntity(entity);
  }

  public void Start() {
    if (!IsClient() && !IsServer()) {
      StartServer();
    }

    if (IsServer()) {
      var input = _input;
      input.playerId = input.GetNewPlayerId();
      if (input.playerId != NetcodeCommon.ServerPlayerId) {
        Debug.LogError("Bad Server Player ID created");
      }

      AddPlayerDataEntity(input.playerId);
    } else {
      var hello = new HelloServerMessage { };
      client.QueueSendToServer(hello);
    }
  }

  public void AddPlayerDataEntity(int playerId) {
    var playerEntity = EntityManager.CreateEntity();
    EntityManager.AddComponentData(playerEntity, new PlayerData {
      id = playerId,
    });
  }

  public Entity GetPlayerEntity() {
    var playerEntities = _playersQuery.ToEntityArray(Allocator.TempJob);
    var players = _playersQuery.ToComponentDataArray<PlayerData>(Allocator.TempJob);

    var result = Entity.Null;
    for (int i = 0; i < players.Length; i++) {
      if (players[i].id == _input.playerId) {
        result = playerEntities[i];
        break;
      }
    }

    players.Dispose();
    playerEntities.Dispose();

    return result;
  }

  public void StartGame(List<PlayerSpec> playerSpecs) {
    if (!IsServer()) {
      throw new InvalidOperationException();
    }

    foreach (var spec in playerSpecs) {
      var entity = InstantiateObject(new NetcodeObjectInitializer {
        id = GetNextNetcodeObjectId(),
        position = spec.startingLocation,
        rotation = 0,
        linearVelocity = float2.zero,
        angularVelocity = 0,
        colorIndex = spec.colorIndex,
        playerId = spec.playerId,
        prefabIndex = spec.prefabIndex,
        isDeathPrefab = false,
      });
      EntityManager.AddComponentData(entity, new PlayerStartLocation {
        value = spec.startingLocation
      });
    }
    
    Object.Destroy(NetcodeLobbySystem.instance);
  }
}

internal struct SyncScore : IComponentData {
  public float value;
}