﻿using System;
using System.Collections.Generic;
using System.IO;
using Unity.Entities;
using Unity.Jobs.LowLevel.Unsafe;
using Unity.Mathematics;
using UnityEngine;

public class GameManager : MonoBehaviour {
  public static GameManager instance;
  public Texture2D cursorTexture;
  public List<Color> playerColors;
  
  private int _playerIndex;

  private SimulationSystemGroup _simGroup;
  private FixedStepSimulationSystemGroup _fixedSimGroup;
  private PrePhysicsGroup _preSimGroup;
  private PostPhysicsGroup _postSimGroup;
  private Camera _camera;
  private World _world;
  private GameSystem _game;
  private int _highestSeenServerTick;
  
  private StreamWriter _log;
  private float _targetTickFloat;

  private const float TickDeltaApproach = .1f;

  // Start is called before the first frame update
  void Start() {
    instance = this;
    _camera = Camera.main;
    _world = World.DefaultGameObjectInjectionWorld;
    _game = _world.GetExistingSystem<GameSystem>();
    _preSimGroup = _world.GetExistingSystem<PrePhysicsGroup>();
    _preSimGroup.Enabled = false;
    _simGroup = _world.GetExistingSystem<SimulationSystemGroup>();
    _simGroup.Enabled = false;
    _fixedSimGroup = _world.GetExistingSystem<FixedStepSimulationSystemGroup>();
    _fixedSimGroup.Enabled = false;
    _postSimGroup = _world.GetExistingSystem<PostPhysicsGroup>();
    _postSimGroup.Enabled = false;
    _camera.transparencySortMode = TransparencySortMode.CustomAxis;
    _camera.transparencySortAxis = _camera.projectionMatrix * Vector3.down;
    Debug.Log($"Logging to \"${Application.persistentDataPath}\"");
    _log = File.CreateText(Application.persistentDataPath + "/logfile-" + DateTime.Now.ToFileTimeUtc() +  ".txt");
    Application.logMessageReceived += ApplicationOnlogMessageReceived;
    
    Screen.SetResolution(800, 450, false);
    JobsUtility.JobWorkerCount = 1;
    Cursor.SetCursor(cursorTexture, Vector2.zero, CursorMode.Auto);
  }

  public Color PlayerColor(int playerId) {
    return playerColors[playerId % playerColors.Count];
  }

  private void ApplicationOnlogMessageReceived(string condition, string stacktrace, LogType type) {
    _log.Write(type.ToString());
    _log.Write(condition);
    _log.Write(stacktrace);
    _log.Write("\n");
    _log.Flush();
  }

  private void FixedUpdate() {
    if (_game.IsClient()) {
      _game.client.Tick(NetcodeTickMode.ReceiveOnly);
    } else if (_game.IsServer()) {
      _game.server.Tick(NetcodeTickMode.ReceiveOnly);
    }

    if (Input.GetKey(KeyCode.P)) {
      return;
    }
    
    int advanceTimes = 1;
    if (_game.IsClient()) {
      var tickDelta = _highestSeenServerTick - _targetTickFloat;
      if (tickDelta < 0) {
        // do nothing
      } else if (tickDelta < 5f) {
        _targetTickFloat += 1f;
      } else if (tickDelta < 10f) {
        _targetTickFloat += 1.2f;
      } else if (tickDelta < 20f) {
        _targetTickFloat += 1.4f;
      } else if (tickDelta < 40f) {
        _targetTickFloat += 1.6f;
      } else if (tickDelta < 80f) {
        _targetTickFloat += 1.8f;
      } else if (tickDelta < 160f) {
        _targetTickFloat += 2f;
      } else {
        _targetTickFloat = _highestSeenServerTick;
        ServerTickStatic.serverTick = _highestSeenServerTick;
      }

      advanceTimes = ((int) _targetTickFloat) - ServerTickStatic.serverTick;
    }
    
    for (int i = 0; i < advanceTimes; i++) {
      if (_game.IsServer()) {
        ServerTickStatic.serverTick++;
        _game.server.QueueBroadcast(new ServerTickMessage {
          serverTick = ServerTickStatic.serverTick,
        }, true);
      } else {
        ServerTickStatic.serverTick++;
      }
      _preSimGroup.Enabled = true;
      _preSimGroup.Update();
      _preSimGroup.Enabled = false;
      _simGroup.Enabled = true;
      _simGroup.Update();
      _simGroup.Enabled = false; 
      _fixedSimGroup.Enabled = true;
      _fixedSimGroup.Update();
      _fixedSimGroup.Enabled = false; 
      _postSimGroup.Enabled = true;
      _postSimGroup.Update();
      _postSimGroup.Enabled = false;
    }
    
    if (_game.IsClient()) {
      _game.client.Tick(NetcodeTickMode.SendAndReceive);
    } else if (_game.IsServer()) {
      _game.server.Tick(NetcodeTickMode.SendAndReceive);
    }
  }

  public void ProcessServerTick(int serverTick) {
    _highestSeenServerTick = math.max(serverTick, _highestSeenServerTick);
  }
}
