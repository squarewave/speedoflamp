﻿using Unity.Entities;
using Unity.Mathematics;

public struct PlayerStartLocation : IComponentData {
  public float2 value;
}
