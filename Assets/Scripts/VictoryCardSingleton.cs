﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class VictoryCardSingleton : MonoBehaviour {
  public static VictoryCardSingleton instance;

  private void Start() {
    instance = this;
    gameObject.SetActive(false);
  }
  
  public void MainMenu() {
    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
  }
}