﻿using System;
using System.Collections.Generic;

public static class SlowRandom {
  private static readonly Random Rng = new Random();

  // Just does a fisher-yates shuffle using a C# Random. Don't put this in a hot loop.
  public static void SlowShuffle<T>(this IList<T> list) {  
    int n = list.Count;  
    while (n > 1) {  
      n--;  
      int k = Rng.Next(n + 1);  
      T value = list[k];  
      list[k] = list[n];  
      list[n] = value;  
    }  
  }
}
