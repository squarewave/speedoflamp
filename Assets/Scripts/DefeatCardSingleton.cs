﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class DefeatCardSingleton : MonoBehaviour {
  public static DefeatCardSingleton instance;

  private void Start() {
    instance = this;
    gameObject.SetActive(false);
  }

  public void MainMenu() {
    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
  }
}
