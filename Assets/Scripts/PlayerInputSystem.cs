using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Extensions;
using Unity.Physics.Systems;
using Unity.Profiling;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = Unity.Mathematics.Random;

[UpdateInGroup(typeof(PrePhysicsGroup))]
public class PlayerInputSystem : SystemBase
{
    private GameSystem _game;
    private GameInputSystem _input;
    private BuildPhysicsWorld _buildPhysicsWorld;
    private const int ProjectileExpiryTicks = 60;
    private Vector3 _cameraVelocity;
    private Entity _winningPlayer = Entity.Null;

    protected override void OnCreate() {
        SceneManager.sceneUnloaded += SceneManagerOnsceneUnloaded;
        
        _game = World.GetOrCreateSystem<GameSystem>();
        _input = World.GetOrCreateSystem<GameInputSystem>();
        _buildPhysicsWorld = World.GetOrCreateSystem<BuildPhysicsWorld>();
    }

    private void SceneManagerOnsceneUnloaded(Scene s) {
        _cameraVelocity = Vector3.zero;
        _winningPlayer = Entity.Null;
    }

    private struct IgnoreSelfCollector : ICollector<ColliderCastHit> {
        private readonly int _selfRigidBodyIndex;
        private readonly float3 _selfPosition;
        public ColliderCastHit closestHit;
        public float closestDistancesq;

        public bool EarlyOutOnFirstHit => false;
        public float MaxFraction => 1f;
        public int NumHits { get; private set; }

        public IgnoreSelfCollector(int selfRigidBodyIndex, float3 selfPosition) {
            NumHits = 0;
            _selfRigidBodyIndex = selfRigidBodyIndex;
            _selfPosition = selfPosition;
            closestHit = default;
            closestDistancesq = float.MaxValue;
        }

        public bool AddHit(ColliderCastHit hit) {
            if (hit.RigidBodyIndex == _selfRigidBodyIndex) {
                return false;
            }

            float distancesq = math.distancesq(hit.Position, _selfPosition);
            if (distancesq < closestDistancesq) {
                closestHit = hit;
                closestDistancesq = distancesq;
            }

            NumHits = 1;
            return true;
        }
    }

    struct ProjectileCreation {
        public float3 translation;
        public float3 impulse;
        public int playerId;
    }

    
    static readonly ProfilerMarker PlayerInputSystemUpdate = new ProfilerMarker("PlayerInputSystem.OnUpdate");
    private EntityQuery _mainQuery;

    protected override void OnUpdate() {
        using var updateScope = PlayerInputSystemUpdate.Auto();
        NativeList<PlayerInput> inputs = new NativeList<PlayerInput>(Allocator.Temp);
        _input.UnqueueInputs(inputs);
        
        var prefabList = GetSingleton<NetcodePrefabList>().prefabs;
        var projectilePrefab = EntityManager.GetBuffer<NetcodePrefabBuffer>(prefabList)[(int)AllNetcodePrefabs.Projectile].value;
        var projectileMass = EntityManager.GetComponentData<PhysicsMass>(projectilePrefab);
        
        NativeList<ProjectileCreation> projectilesToCreate = new NativeList<ProjectileCreation>(Allocator.Temp);
        
        float3 playerPosition = float3.zero;

        var enableShooting = new NativeArray<Entity>(_mainQuery.CalculateEntityCount(), Allocator.Temp);
        var disableShooting = new NativeArray<Entity>(_mainQuery.CalculateEntityCount(), Allocator.Temp);
        var finishLineBounds = FinishLineSingleton.instance.GetBounds();

        int inputPlayerId = _input.playerId;
        if (InputSingleton.instance.playerEntity == Entity.Null) {
            Entities
                .WithoutBurst()
                .ForEach((Entity entity,
                          in PlayerInputData input,
                          in NetcodeObject netObj) => {
                    if (netObj.playerId == inputPlayerId) {
                        InputSingleton.instance.playerEntity = entity;
                    }
                }).Run();
        }

        int serverTick = ServerTickStatic.serverTick;
        var winningPlayer = _winningPlayer;
        Entities
            .ForEach((Entity entity,
                      int entityInQueryIndex,
                      ref PhysicsVelocity velocity,
                      ref PlayerInputData input,
                      ref PhysicsCollider collider,
                      in Translation translation,
                      in PhysicsMass mass,
                      in NetcodeObject netObj) => {
                if (winningPlayer == Entity.Null && finishLineBounds.Contains(translation.Value)) {
                    winningPlayer = entity;
                }
                if (netObj.playerId == inputPlayerId) {
                    playerPosition = translation.Value;
                }

                for (int i = 0; i < inputs.Length; i++) {
                    var playerInput = inputs[i];
                    if (playerInput.playerId != netObj.playerId) {
                        continue;
                    }

                    switch (playerInput.command) {
                        case InputCommand.shoot: {
                            if (!input.shooting) {
                                enableShooting[entityInQueryIndex] = entity;
                                input.shooting = true;   
                            }
                            math.sincos(playerInput.payload, out float sin, out float cos);
                            input.shootTarget = new float2(cos, sin);
                            break;
                        }
                        case InputCommand.shootStop: {
                            if (input.shooting) {
                                disableShooting[entityInQueryIndex] = entity;
                                input.shooting = false;   
                            }
                            break;
                        }
                    }
                }

                if(input.shooting) {
                    float3 launcherDirection = translation.Value + new float3(input.shootTarget.x, input.shootTarget.y, 0f);

                    float3 impulseDirection = translation.Value - launcherDirection;
                    impulseDirection = math.normalize(impulseDirection);

                    velocity.ApplyLinearImpulse(mass, impulseDirection * input.recoilImpulse);
                    var rand = Random.CreateFromIndex((uint)serverTick);
                    var offset = new float3 {
                        x = rand.NextFloat() * 0.2f,
                        y = rand.NextFloat() * 0.2f,
                        z = rand.NextFloat() * 0.2f,
                    };
                    projectilesToCreate.Add(new ProjectileCreation {
                        impulse = (impulseDirection + offset) * input.recoilImpulse * -6f,
                        translation = translation.Value - impulseDirection * 0.5f,
                        playerId = netObj.playerId,
                    });
                }
            }).WithStoreEntityQueryInField(ref _mainQuery)
            .Run();
        _winningPlayer = winningPlayer;

        Camera.main.transform.position = Vector3.SmoothDamp(Camera.main.transform.position,
                                                            new Vector3(playerPosition.x, playerPosition.y, -10), ref _cameraVelocity,
                                                            .2f);

        if (_winningPlayer != Entity.Null) {
            var playerId = EntityManager.GetComponentData<NetcodeObject>(_winningPlayer).playerId;
            if (playerId == inputPlayerId) {
                VictoryCardSingleton.instance.gameObject.SetActive(true);
            } else {
                DefeatCardSingleton.instance.gameObject.SetActive(true);
            }
        }

        foreach (var entity in enableShooting) {
            if (entity == Entity.Null) {
                continue;
            }
            EntityManager.GetComponentObject<GameObject>(entity)
                         .GetComponent<AudioSource>().enabled = true;
        }

        foreach (var entity in disableShooting) {
            if (entity == Entity.Null) {
                continue;
            }
            EntityManager.GetComponentObject<GameObject>(entity)
                         .GetComponent<AudioSource>().enabled = false;
        }

        foreach (var p in projectilesToCreate) {
            var projectileVelocity = new PhysicsVelocity();
            projectileVelocity.ApplyLinearImpulse(projectileMass, p.impulse);
        
            var projectile = _game.InstantiateObject(new NetcodeObjectInitializer {
                id = _game.GetNextNetcodeObjectId(),
                position = p.translation.xy,
                rotation = 0,
                linearVelocity = projectileVelocity.Linear.xy,
                angularVelocity = projectileVelocity.Angular.z,
                playerId = p.playerId,
                expiresOnTick = serverTick + 60,
                prefabIndex = (int) AllNetcodePrefabs.Projectile,
                isDeathPrefab = false
            });
            EntityManager.SetComponentData(projectile, projectileVelocity);
        }
    }
}
