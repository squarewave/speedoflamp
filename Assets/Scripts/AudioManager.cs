using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour {
    public AudioMixer mixer;
    public Slider musicSlider;
    public Slider sfxSlider;

    private void Start() {
        var music = PlayerPrefs.GetFloat("musicVol");
        musicSlider.value = music;
        var sfx = PlayerPrefs.GetFloat("sfxVol");
        sfxSlider.value = sfx;
    }

    public void SetMusicVol(float value) {
        mixer.SetFloat("musicVol", value);
        PlayerPrefs.SetFloat("musicVol", value);
    }

    public void SetSFXVol(float value) {
        mixer.SetFloat("sfxVol", value);
        PlayerPrefs.SetFloat("sfxVol", value);
    }
}
