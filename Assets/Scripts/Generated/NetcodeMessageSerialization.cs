using Unity.Collections;
using Unity.Entities;
using UnityEngine;

public enum NetcodeMessageId {
  Null,
  BulkInputMessage,
  HelloClientMessage,
  HelloServerMessage,
  ObjectInstantiatedMessage,
  ObjectsDestroyedMessage,
  PingMessage,
  PlayerEnteredMessage,
  PlayerInputMessage,
  PlayerLobbyStateMessage,
  ServerTickMessage,
  TrackedBodiesStateMessage,
  Count,
}

public static partial class NetcodeMessageHandling {
  public static NetcodeMessageId MessageIdForType<T>() {
    if (typeof(T) == typeof(BulkInputMessage)) { return NetcodeMessageId.BulkInputMessage; }
    if (typeof(T) == typeof(HelloClientMessage)) { return NetcodeMessageId.HelloClientMessage; }
    if (typeof(T) == typeof(HelloServerMessage)) { return NetcodeMessageId.HelloServerMessage; }
    if (typeof(T) == typeof(ObjectInstantiatedMessage)) { return NetcodeMessageId.ObjectInstantiatedMessage; }
    if (typeof(T) == typeof(ObjectsDestroyedMessage)) { return NetcodeMessageId.ObjectsDestroyedMessage; }
    if (typeof(T) == typeof(PingMessage)) { return NetcodeMessageId.PingMessage; }
    if (typeof(T) == typeof(PlayerEnteredMessage)) { return NetcodeMessageId.PlayerEnteredMessage; }
    if (typeof(T) == typeof(PlayerInputMessage)) { return NetcodeMessageId.PlayerInputMessage; }
    if (typeof(T) == typeof(PlayerLobbyStateMessage)) { return NetcodeMessageId.PlayerLobbyStateMessage; }
    if (typeof(T) == typeof(ServerTickMessage)) { return NetcodeMessageId.ServerTickMessage; }
    if (typeof(T) == typeof(TrackedBodiesStateMessage)) { return NetcodeMessageId.TrackedBodiesStateMessage; }
    return NetcodeMessageId.Null;
  }

  public static bool HandleMessage(World world,
                                   bool isServer,
                                   NetcodeMessageHeader header,
                                   int playerId,
                                   NativeArray<byte> buffer,
                                   ref int offset) {
    switch (header.messageTypeId) {
       
      case NetcodeMessageId.BulkInputMessage: {
        int startOffset = offset;
        int bitOffset = offset * 8;
        if (!NetcodeMessageSerialization.Deserialize(buffer, ref bitOffset, Allocator.Persistent,
                                                     NetcodeCommon.PendingPacketRingBufferMask,
                                                     out BulkInputMessage message)) {
          Debug.LogError("Failed to deserialize message of type " + header.messageTypeId);
          return false;
        }
        offset = bitOffset / 8 + (bitOffset % 8 != 0 ? 1 : 0);
        if (offset - startOffset + NetcodeMessageHeader.headerSize != header.size) {
          Debug.LogError(
            $"Incorrectly sized message (deserialized: {offset - startOffset + NetcodeMessageHeader.headerSize} header-reported: {header.size}) (type: {header.messageTypeId})");
        }
        return HandleMessage<BulkInputMessage>(ref message, world, isServer, playerId, buffer, ref offset);
      }
      case NetcodeMessageId.HelloClientMessage: {
        int startOffset = offset;
        int bitOffset = offset * 8;
        if (!NetcodeMessageSerialization.Deserialize(buffer, ref bitOffset, Allocator.Persistent,
                                                     NetcodeCommon.PendingPacketRingBufferMask,
                                                     out HelloClientMessage message)) {
          Debug.LogError("Failed to deserialize message of type " + header.messageTypeId);
          return false;
        }
        offset = bitOffset / 8 + (bitOffset % 8 != 0 ? 1 : 0);
        if (offset - startOffset + NetcodeMessageHeader.headerSize != header.size) {
          Debug.LogError(
            $"Incorrectly sized message (deserialized: {offset - startOffset + NetcodeMessageHeader.headerSize} header-reported: {header.size}) (type: {header.messageTypeId})");
        }
        return HandleMessage<HelloClientMessage>(ref message, world, isServer, playerId, buffer, ref offset);
      }
      case NetcodeMessageId.HelloServerMessage: {
        int startOffset = offset;
        int bitOffset = offset * 8;
        if (!NetcodeMessageSerialization.Deserialize(buffer, ref bitOffset, Allocator.Persistent,
                                                     NetcodeCommon.PendingPacketRingBufferMask,
                                                     out HelloServerMessage message)) {
          Debug.LogError("Failed to deserialize message of type " + header.messageTypeId);
          return false;
        }
        offset = bitOffset / 8 + (bitOffset % 8 != 0 ? 1 : 0);
        if (offset - startOffset + NetcodeMessageHeader.headerSize != header.size) {
          Debug.LogError(
            $"Incorrectly sized message (deserialized: {offset - startOffset + NetcodeMessageHeader.headerSize} header-reported: {header.size}) (type: {header.messageTypeId})");
        }
        return HandleMessage<HelloServerMessage>(ref message, world, isServer, playerId, buffer, ref offset);
      }
      case NetcodeMessageId.ObjectInstantiatedMessage: {
        int startOffset = offset;
        int bitOffset = offset * 8;
        if (!NetcodeMessageSerialization.Deserialize(buffer, ref bitOffset, Allocator.Persistent,
                                                     NetcodeCommon.PendingPacketRingBufferMask,
                                                     out ObjectInstantiatedMessage message)) {
          Debug.LogError("Failed to deserialize message of type " + header.messageTypeId);
          return false;
        }
        offset = bitOffset / 8 + (bitOffset % 8 != 0 ? 1 : 0);
        if (offset - startOffset + NetcodeMessageHeader.headerSize != header.size) {
          Debug.LogError(
            $"Incorrectly sized message (deserialized: {offset - startOffset + NetcodeMessageHeader.headerSize} header-reported: {header.size}) (type: {header.messageTypeId})");
        }
        return HandleMessage<ObjectInstantiatedMessage>(ref message, world, isServer, playerId, buffer, ref offset);
      }
      case NetcodeMessageId.ObjectsDestroyedMessage: {
        int startOffset = offset;
        int bitOffset = offset * 8;
        if (!NetcodeMessageSerialization.Deserialize(buffer, ref bitOffset, Allocator.Persistent,
                                                     NetcodeCommon.PendingPacketRingBufferMask,
                                                     out ObjectsDestroyedMessage message)) {
          Debug.LogError("Failed to deserialize message of type " + header.messageTypeId);
          return false;
        }
        offset = bitOffset / 8 + (bitOffset % 8 != 0 ? 1 : 0);
        if (offset - startOffset + NetcodeMessageHeader.headerSize != header.size) {
          Debug.LogError(
            $"Incorrectly sized message (deserialized: {offset - startOffset + NetcodeMessageHeader.headerSize} header-reported: {header.size}) (type: {header.messageTypeId})");
        }
        return HandleMessage<ObjectsDestroyedMessage>(ref message, world, isServer, playerId, buffer, ref offset);
      }
      case NetcodeMessageId.PingMessage: {
        int startOffset = offset;
        int bitOffset = offset * 8;
        if (!NetcodeMessageSerialization.Deserialize(buffer, ref bitOffset, Allocator.Persistent,
                                                     NetcodeCommon.PendingPacketRingBufferMask,
                                                     out PingMessage message)) {
          Debug.LogError("Failed to deserialize message of type " + header.messageTypeId);
          return false;
        }
        offset = bitOffset / 8 + (bitOffset % 8 != 0 ? 1 : 0);
        if (offset - startOffset + NetcodeMessageHeader.headerSize != header.size) {
          Debug.LogError(
            $"Incorrectly sized message (deserialized: {offset - startOffset + NetcodeMessageHeader.headerSize} header-reported: {header.size}) (type: {header.messageTypeId})");
        }
        return HandleMessage<PingMessage>(ref message, world, isServer, playerId, buffer, ref offset);
      }
      case NetcodeMessageId.PlayerEnteredMessage: {
        int startOffset = offset;
        int bitOffset = offset * 8;
        if (!NetcodeMessageSerialization.Deserialize(buffer, ref bitOffset, Allocator.Persistent,
                                                     NetcodeCommon.PendingPacketRingBufferMask,
                                                     out PlayerEnteredMessage message)) {
          Debug.LogError("Failed to deserialize message of type " + header.messageTypeId);
          return false;
        }
        offset = bitOffset / 8 + (bitOffset % 8 != 0 ? 1 : 0);
        if (offset - startOffset + NetcodeMessageHeader.headerSize != header.size) {
          Debug.LogError(
            $"Incorrectly sized message (deserialized: {offset - startOffset + NetcodeMessageHeader.headerSize} header-reported: {header.size}) (type: {header.messageTypeId})");
        }
        return HandleMessage<PlayerEnteredMessage>(ref message, world, isServer, playerId, buffer, ref offset);
      }
      case NetcodeMessageId.PlayerInputMessage: {
        int startOffset = offset;
        int bitOffset = offset * 8;
        if (!NetcodeMessageSerialization.Deserialize(buffer, ref bitOffset, Allocator.Persistent,
                                                     NetcodeCommon.PendingPacketRingBufferMask,
                                                     out PlayerInputMessage message)) {
          Debug.LogError("Failed to deserialize message of type " + header.messageTypeId);
          return false;
        }
        offset = bitOffset / 8 + (bitOffset % 8 != 0 ? 1 : 0);
        if (offset - startOffset + NetcodeMessageHeader.headerSize != header.size) {
          Debug.LogError(
            $"Incorrectly sized message (deserialized: {offset - startOffset + NetcodeMessageHeader.headerSize} header-reported: {header.size}) (type: {header.messageTypeId})");
        }
        return HandleMessage<PlayerInputMessage>(ref message, world, isServer, playerId, buffer, ref offset);
      }
      case NetcodeMessageId.PlayerLobbyStateMessage: {
        int startOffset = offset;
        int bitOffset = offset * 8;
        if (!NetcodeMessageSerialization.Deserialize(buffer, ref bitOffset, Allocator.Persistent,
                                                     NetcodeCommon.PendingPacketRingBufferMask,
                                                     out PlayerLobbyStateMessage message)) {
          Debug.LogError("Failed to deserialize message of type " + header.messageTypeId);
          return false;
        }
        offset = bitOffset / 8 + (bitOffset % 8 != 0 ? 1 : 0);
        if (offset - startOffset + NetcodeMessageHeader.headerSize != header.size) {
          Debug.LogError(
            $"Incorrectly sized message (deserialized: {offset - startOffset + NetcodeMessageHeader.headerSize} header-reported: {header.size}) (type: {header.messageTypeId})");
        }
        return HandleMessage<PlayerLobbyStateMessage>(ref message, world, isServer, playerId, buffer, ref offset);
      }
      case NetcodeMessageId.ServerTickMessage: {
        int startOffset = offset;
        int bitOffset = offset * 8;
        if (!NetcodeMessageSerialization.Deserialize(buffer, ref bitOffset, Allocator.Persistent,
                                                     NetcodeCommon.PendingPacketRingBufferMask,
                                                     out ServerTickMessage message)) {
          Debug.LogError("Failed to deserialize message of type " + header.messageTypeId);
          return false;
        }
        offset = bitOffset / 8 + (bitOffset % 8 != 0 ? 1 : 0);
        if (offset - startOffset + NetcodeMessageHeader.headerSize != header.size) {
          Debug.LogError(
            $"Incorrectly sized message (deserialized: {offset - startOffset + NetcodeMessageHeader.headerSize} header-reported: {header.size}) (type: {header.messageTypeId})");
        }
        return HandleMessage<ServerTickMessage>(ref message, world, isServer, playerId, buffer, ref offset);
      }
      case NetcodeMessageId.TrackedBodiesStateMessage: {
        int startOffset = offset;
        int bitOffset = offset * 8;
        if (!NetcodeMessageSerialization.Deserialize(buffer, ref bitOffset, Allocator.Persistent,
                                                     NetcodeCommon.PendingPacketRingBufferMask,
                                                     out TrackedBodiesStateMessage message)) {
          Debug.LogError("Failed to deserialize message of type " + header.messageTypeId);
          return false;
        }
        offset = bitOffset / 8 + (bitOffset % 8 != 0 ? 1 : 0);
        if (offset - startOffset + NetcodeMessageHeader.headerSize != header.size) {
          Debug.LogError(
            $"Incorrectly sized message (deserialized: {offset - startOffset + NetcodeMessageHeader.headerSize} header-reported: {header.size}) (type: {header.messageTypeId})");
        }
        return HandleMessage<TrackedBodiesStateMessage>(ref message, world, isServer, playerId, buffer, ref offset);
      }
    }

    return false;
  }
}

public static partial class NetcodeMessageSerialization {

  public static bool Serialize(BulkInputMessage msg,
                               NativeArray<byte> buffer,
                               ref int bitOffset,
                               int bufferMask) {

    if (!Quantization.Quantize(msg.serverTick, buffer, ref bitOffset, bufferMask, 32, -256, 2147483648)) {
      return false;
    }
    if (!Quantization.Quantize(msg.inputs.Length, buffer, ref bitOffset, bufferMask, 16, 0, 1 << 16)) {
      return false;
    }
    
    foreach (var entry in msg.inputs) {
      if (!Serialize(entry, buffer, ref bitOffset, bufferMask)) {
        return false;
      }
    }

    return true;
  }
  public static bool Serialize(HelloClientMessage msg,
                               NativeArray<byte> buffer,
                               ref int bitOffset,
                               int bufferMask) {

    if (!Quantization.Quantize(msg.playerId, buffer, ref bitOffset, bufferMask, 6, -1, 62)) {
      return false;
    }
    if (!Quantization.Quantize(msg.netcodeObjectInitializers.Length, buffer, ref bitOffset, bufferMask, 16, 0, 1 << 16)) {
      return false;
    }
    
    foreach (var entry in msg.netcodeObjectInitializers) {
      if (!Serialize(entry, buffer, ref bitOffset, bufferMask)) {
        return false;
      }
    }
    if (!Quantization.Quantize(msg.childIds.Length, buffer, ref bitOffset, bufferMask, 16, 0, 1 << 16)) {
      return false;
    }
    
    foreach (var entry in msg.childIds) {
      if (!Quantization.Quantize(entry, buffer, ref bitOffset, bufferMask, 20, 0, 1048576)) {
        return false;
      }
    }
    if (!Quantization.Quantize(msg.players.Length, buffer, ref bitOffset, bufferMask, 16, 0, 1 << 16)) {
      return false;
    }
    
    foreach (var entry in msg.players) {
      if (!Serialize(entry, buffer, ref bitOffset, bufferMask)) {
        return false;
      }
    }

    return true;
  }
  public static bool Serialize(HelloServerMessage msg,
                               NativeArray<byte> buffer,
                               ref int bitOffset,
                               int bufferMask) {


    return true;
  }
  public static bool Serialize(ObjectInstantiatedMessage msg,
                               NativeArray<byte> buffer,
                               ref int bitOffset,
                               int bufferMask) {

    if (!Serialize(msg.initializer, buffer, ref bitOffset, bufferMask)) {
      return false;
    }
    if (!Quantization.Quantize(msg.childIds.Length, buffer, ref bitOffset, bufferMask, 16, 0, 1 << 16)) {
      return false;
    }
    
    foreach (var entry in msg.childIds) {
      if (!Quantization.Quantize(entry, buffer, ref bitOffset, bufferMask, 20, 0, 1048576)) {
        return false;
      }
    }
    if (!Quantization.Quantize(msg.serverTick, buffer, ref bitOffset, bufferMask, 32, -256, 2147483648)) {
      return false;
    }

    return true;
  }
  public static bool Serialize(ObjectsDestroyedMessage msg,
                               NativeArray<byte> buffer,
                               ref int bitOffset,
                               int bufferMask) {

    if (!Quantization.Quantize(msg.objectIds.Length, buffer, ref bitOffset, bufferMask, 16, 0, 1 << 16)) {
      return false;
    }
    
    foreach (var entry in msg.objectIds) {
      if (!Quantization.Quantize(entry, buffer, ref bitOffset, bufferMask, 20, 0, 1048576)) {
        return false;
      }
    }

    return true;
  }
  public static bool Serialize(PingMessage msg,
                               NativeArray<byte> buffer,
                               ref int bitOffset,
                               int bufferMask) {

    if (!Quantization.Quantize(msg.estimatedServerTick, buffer, ref bitOffset, bufferMask, 32, -256, 2147483648)) {
      return false;
    }
    if (!Quantization.Quantize(msg.lastPingTime, buffer, ref bitOffset, bufferMask, 32, 0, 1000000)) {
      return false;
    }

    return true;
  }
  public static bool Serialize(PlayerEnteredMessage msg,
                               NativeArray<byte> buffer,
                               ref int bitOffset,
                               int bufferMask) {

    if (!Quantization.Quantize(msg.playerId, buffer, ref bitOffset, bufferMask, 6, -1, 62)) {
      return false;
    }

    return true;
  }
  public static bool Serialize(PlayerInputMessage msg,
                               NativeArray<byte> buffer,
                               ref int bitOffset,
                               int bufferMask) {

    if (!Serialize(msg.playerInput, buffer, ref bitOffset, bufferMask)) {
      return false;
    }

    return true;
  }
  public static bool Serialize(PlayerLobbyStateMessage msg,
                               NativeArray<byte> buffer,
                               ref int bitOffset,
                               int bufferMask) {

    if (!Quantization.Quantize(msg.playerIds.Length, buffer, ref bitOffset, bufferMask, 16, 0, 1 << 16)) {
      return false;
    }
    
    foreach (var entry in msg.playerIds) {
      if (!Quantization.Quantize(entry, buffer, ref bitOffset, bufferMask, 6, -1, 62)) {
        return false;
      }
    }

    return true;
  }
  public static bool Serialize(ServerTickMessage msg,
                               NativeArray<byte> buffer,
                               ref int bitOffset,
                               int bufferMask) {

    if (!Quantization.Quantize(msg.serverTick, buffer, ref bitOffset, bufferMask, 32, -256, 2147483648)) {
      return false;
    }

    return true;
  }
  public static bool Serialize(TrackedBodiesStateMessage msg,
                               NativeArray<byte> buffer,
                               ref int bitOffset,
                               int bufferMask) {

    if (!Quantization.Quantize(msg.serverTick, buffer, ref bitOffset, bufferMask, 32, -256, 2147483648)) {
      return false;
    }
    if (!Quantization.Quantize(msg.trackedBodies.Length, buffer, ref bitOffset, bufferMask, 16, 0, 1 << 16)) {
      return false;
    }
    
    foreach (var entry in msg.trackedBodies) {
      if (!Serialize(entry, buffer, ref bitOffset, bufferMask)) {
        return false;
      }
    }

    return true;
  }
  public static bool Serialize(MiniInputState msg,
                               NativeArray<byte> buffer,
                               ref int bitOffset,
                               int bufferMask) {

    int __commandInt = (int)msg.command;
    if (!Quantization.Quantize(__commandInt, buffer, ref bitOffset, bufferMask, -1, 0, 2)) {
      return false;
    }
    if (!Quantization.Quantize(msg.aimDirection, buffer, ref bitOffset, bufferMask, QuantizationConstants.InputPayloadBits, QuantizationConstants.InputPayloadMin, QuantizationConstants.InputPayloadMax)) {
      return false;
    }

    return true;
  }
  public static bool Serialize(NetcodeObjectInitializer msg,
                               NativeArray<byte> buffer,
                               ref int bitOffset,
                               int bufferMask) {

    if (!Quantization.Quantize(msg.id, buffer, ref bitOffset, bufferMask, 20, 0, 1048576)) {
      return false;
    }
    if (!Quantization.Quantize(msg.playerId, buffer, ref bitOffset, bufferMask, 6, -1, 62)) {
      return false;
    }
    if (!Quantization.Quantize(msg.prefabIndex, buffer, ref bitOffset, bufferMask, 9, 0, 512)) {
      return false;
    }
    if (!Quantization.Quantize(msg.position, buffer, ref bitOffset, bufferMask, TrackedBodyState.RelPositionBits, TrackedBodyState.RelPositionMin, TrackedBodyState.RelPositionMax)) {
      return false;
    }
    if (!Quantization.Quantize(msg.rotation, buffer, ref bitOffset, bufferMask, TrackedBodyState.RotationBits, TrackedBodyState.RotationMin, TrackedBodyState.RotationMax)) {
      return false;
    }
    if (!Quantization.Quantize(msg.linearVelocity, buffer, ref bitOffset, bufferMask, TrackedBodyState.VelocityBits, TrackedBodyState.VelocityMin, TrackedBodyState.VelocityMax)) {
      return false;
    }
    if (!Quantization.Quantize(msg.angularVelocity, buffer, ref bitOffset, bufferMask, TrackedBodyState.AngularVelocityBits, TrackedBodyState.AngularVelocityMin, TrackedBodyState.AngularVelocityMax)) {
      return false;
    }
    if (!Quantization.Quantize(msg.expiresOnTick, buffer, ref bitOffset, bufferMask, 32, -256, 2147483648)) {
      return false;
    }
    int __isDeathPrefabInt = msg.isDeathPrefab ? 1 : 0;
    if (!Quantization.Quantize(__isDeathPrefabInt, buffer, ref bitOffset, bufferMask, 1, 0, 2)) {
      return false;
    }
    if (!Quantization.Quantize(msg.colorIndex, buffer, ref bitOffset, bufferMask, 5, 0, 32)) {
      return false;
    }

    return true;
  }
  public static bool Serialize(PlayerInput msg,
                               NativeArray<byte> buffer,
                               ref int bitOffset,
                               int bufferMask) {

    if (!Quantization.Quantize(msg.playerId, buffer, ref bitOffset, bufferMask, 6, -1, 62)) {
      return false;
    }
    if (!Quantization.Quantize(msg.serverTick, buffer, ref bitOffset, bufferMask, 32, -256, 2147483648)) {
      return false;
    }
    int __commandInt = (int)msg.command;
    if (!Quantization.Quantize(__commandInt, buffer, ref bitOffset, bufferMask, -1, 0, 2)) {
      return false;
    }
    if (!Quantization.Quantize(msg.payload, buffer, ref bitOffset, bufferMask, QuantizationConstants.InputPayloadBits, QuantizationConstants.InputPayloadMin, QuantizationConstants.InputPayloadMax)) {
      return false;
    }

    return true;
  }
  public static bool Serialize(TrackedBodyState msg,
                               NativeArray<byte> buffer,
                               ref int bitOffset,
                               int bufferMask) {

    if (!Quantization.Quantize(msg.id, buffer, ref bitOffset, bufferMask, 20, 0, 1048576)) {
      return false;
    }
    if (!Quantization.Quantize(msg.position, buffer, ref bitOffset, bufferMask, TrackedBodyState.RelPositionBits, TrackedBodyState.RelPositionMin, TrackedBodyState.RelPositionMax)) {
      return false;
    }
    if (!Quantization.Quantize(msg.velocity, buffer, ref bitOffset, bufferMask, TrackedBodyState.VelocityBits, TrackedBodyState.VelocityMin, TrackedBodyState.VelocityMax)) {
      return false;
    }
    if (!Quantization.Quantize(msg.angularVelocity, buffer, ref bitOffset, bufferMask, TrackedBodyState.AngularVelocityBits, TrackedBodyState.AngularVelocityMin, TrackedBodyState.AngularVelocityMax)) {
      return false;
    }
    if (!Quantization.Quantize(msg.rotation, buffer, ref bitOffset, bufferMask, TrackedBodyState.RotationBits, TrackedBodyState.RotationMin, TrackedBodyState.RotationMax)) {
      return false;
    }

    return true;
  }
  public static bool Serialize(PlayerData msg,
                               NativeArray<byte> buffer,
                               ref int bitOffset,
                               int bufferMask) {

    if (!Quantization.Quantize(msg.id, buffer, ref bitOffset, bufferMask, 6, -1, 62)) {
      return false;
    }

    return true;
  }

  public static bool Deserialize(NativeArray<byte> buffer,
                                 ref int bitOffset,
                                 Allocator allocator,
                                 int bufferMask,
                                 out BulkInputMessage msg) {
    msg = default;

    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, 32, -256, 2147483648, out System.Int32 serverTickValue)) {
      return false;
    }
    msg.serverTick = serverTickValue;
    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, 16, 0, 1 << 16, out var inputsLength)) {
      return false;
    }
    msg.inputs = new NativeArray<MiniInputState>(inputsLength, allocator);

    
    for (int i = 0; i < inputsLength; i++) {
      if (!Deserialize(buffer, ref bitOffset, allocator, bufferMask, out MiniInputState entry)) {
        return false;
      }
      msg.inputs[i] = entry;
    }

    return true;
  }
  public static bool Deserialize(NativeArray<byte> buffer,
                                 ref int bitOffset,
                                 Allocator allocator,
                                 int bufferMask,
                                 out HelloClientMessage msg) {
    msg = default;

    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, 6, -1, 62, out System.Int32 playerIdValue)) {
      return false;
    }
    msg.playerId = playerIdValue;
    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, 16, 0, 1 << 16, out var netcodeObjectInitializersLength)) {
      return false;
    }
    msg.netcodeObjectInitializers = new NativeArray<NetcodeObjectInitializer>(netcodeObjectInitializersLength, allocator);

    
    for (int i = 0; i < netcodeObjectInitializersLength; i++) {
      if (!Deserialize(buffer, ref bitOffset, allocator, bufferMask, out NetcodeObjectInitializer entry)) {
        return false;
      }
      msg.netcodeObjectInitializers[i] = entry;
    }
    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, 16, 0, 1 << 16, out var childIdsLength)) {
      return false;
    }
    msg.childIds = new NativeArray<System.Int32>(childIdsLength, allocator);

    
    for (int i = 0; i < childIdsLength; i++) {
      if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, 20, 0, 1048576, out System.Int32 entry)) {
        return false;
      }
      msg.childIds[i] = entry;
    }
    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, 16, 0, 1 << 16, out var playersLength)) {
      return false;
    }
    msg.players = new NativeArray<PlayerData>(playersLength, allocator);

    
    for (int i = 0; i < playersLength; i++) {
      if (!Deserialize(buffer, ref bitOffset, allocator, bufferMask, out PlayerData entry)) {
        return false;
      }
      msg.players[i] = entry;
    }

    return true;
  }
  public static bool Deserialize(NativeArray<byte> buffer,
                                 ref int bitOffset,
                                 Allocator allocator,
                                 int bufferMask,
                                 out HelloServerMessage msg) {
    msg = default;


    return true;
  }
  public static bool Deserialize(NativeArray<byte> buffer,
                                 ref int bitOffset,
                                 Allocator allocator,
                                 int bufferMask,
                                 out ObjectInstantiatedMessage msg) {
    msg = default;

    if (!Deserialize(buffer, ref bitOffset, allocator, bufferMask, out NetcodeObjectInitializer initializerValue)) {
      return false;
    }
    msg.initializer = initializerValue;
    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, 16, 0, 1 << 16, out var childIdsLength)) {
      return false;
    }
    msg.childIds = new NativeArray<System.Int32>(childIdsLength, allocator);

    
    for (int i = 0; i < childIdsLength; i++) {
      if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, 20, 0, 1048576, out System.Int32 entry)) {
        return false;
      }
      msg.childIds[i] = entry;
    }
    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, 32, -256, 2147483648, out System.Int32 serverTickValue)) {
      return false;
    }
    msg.serverTick = serverTickValue;

    return true;
  }
  public static bool Deserialize(NativeArray<byte> buffer,
                                 ref int bitOffset,
                                 Allocator allocator,
                                 int bufferMask,
                                 out ObjectsDestroyedMessage msg) {
    msg = default;

    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, 16, 0, 1 << 16, out var objectIdsLength)) {
      return false;
    }
    msg.objectIds = new NativeArray<System.Int32>(objectIdsLength, allocator);

    
    for (int i = 0; i < objectIdsLength; i++) {
      if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, 20, 0, 1048576, out System.Int32 entry)) {
        return false;
      }
      msg.objectIds[i] = entry;
    }

    return true;
  }
  public static bool Deserialize(NativeArray<byte> buffer,
                                 ref int bitOffset,
                                 Allocator allocator,
                                 int bufferMask,
                                 out PingMessage msg) {
    msg = default;

    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, 32, -256, 2147483648, out System.Int32 estimatedServerTickValue)) {
      return false;
    }
    msg.estimatedServerTick = estimatedServerTickValue;
    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, 32, 0, 1000000, out System.Single lastPingTimeValue)) {
      return false;
    }
    msg.lastPingTime = lastPingTimeValue;

    return true;
  }
  public static bool Deserialize(NativeArray<byte> buffer,
                                 ref int bitOffset,
                                 Allocator allocator,
                                 int bufferMask,
                                 out PlayerEnteredMessage msg) {
    msg = default;

    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, 6, -1, 62, out System.Int32 playerIdValue)) {
      return false;
    }
    msg.playerId = playerIdValue;

    return true;
  }
  public static bool Deserialize(NativeArray<byte> buffer,
                                 ref int bitOffset,
                                 Allocator allocator,
                                 int bufferMask,
                                 out PlayerInputMessage msg) {
    msg = default;

    if (!Deserialize(buffer, ref bitOffset, allocator, bufferMask, out PlayerInput playerInputValue)) {
      return false;
    }
    msg.playerInput = playerInputValue;

    return true;
  }
  public static bool Deserialize(NativeArray<byte> buffer,
                                 ref int bitOffset,
                                 Allocator allocator,
                                 int bufferMask,
                                 out PlayerLobbyStateMessage msg) {
    msg = default;

    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, 16, 0, 1 << 16, out var playerIdsLength)) {
      return false;
    }
    msg.playerIds = new NativeArray<System.Int32>(playerIdsLength, allocator);

    
    for (int i = 0; i < playerIdsLength; i++) {
      if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, 6, -1, 62, out System.Int32 entry)) {
        return false;
      }
      msg.playerIds[i] = entry;
    }

    return true;
  }
  public static bool Deserialize(NativeArray<byte> buffer,
                                 ref int bitOffset,
                                 Allocator allocator,
                                 int bufferMask,
                                 out ServerTickMessage msg) {
    msg = default;

    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, 32, -256, 2147483648, out System.Int32 serverTickValue)) {
      return false;
    }
    msg.serverTick = serverTickValue;

    return true;
  }
  public static bool Deserialize(NativeArray<byte> buffer,
                                 ref int bitOffset,
                                 Allocator allocator,
                                 int bufferMask,
                                 out TrackedBodiesStateMessage msg) {
    msg = default;

    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, 32, -256, 2147483648, out System.Int32 serverTickValue)) {
      return false;
    }
    msg.serverTick = serverTickValue;
    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, 16, 0, 1 << 16, out var trackedBodiesLength)) {
      return false;
    }
    msg.trackedBodies = new NativeArray<TrackedBodyState>(trackedBodiesLength, allocator);

    
    for (int i = 0; i < trackedBodiesLength; i++) {
      if (!Deserialize(buffer, ref bitOffset, allocator, bufferMask, out TrackedBodyState entry)) {
        return false;
      }
      msg.trackedBodies[i] = entry;
    }

    return true;
  }
  public static bool Deserialize(NativeArray<byte> buffer,
                                 ref int bitOffset,
                                 Allocator allocator,
                                 int bufferMask,
                                 out MiniInputState msg) {
    msg = default;

    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, -1, 0, 2, out int __commandInt)) {
      return false;
    }

    msg.command = (InputCommand)__commandInt;
    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, QuantizationConstants.InputPayloadBits, QuantizationConstants.InputPayloadMin, QuantizationConstants.InputPayloadMax, out System.Single aimDirectionValue)) {
      return false;
    }
    msg.aimDirection = aimDirectionValue;

    return true;
  }
  public static bool Deserialize(NativeArray<byte> buffer,
                                 ref int bitOffset,
                                 Allocator allocator,
                                 int bufferMask,
                                 out NetcodeObjectInitializer msg) {
    msg = default;

    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, 20, 0, 1048576, out System.Int32 idValue)) {
      return false;
    }
    msg.id = idValue;
    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, 6, -1, 62, out System.Int32 playerIdValue)) {
      return false;
    }
    msg.playerId = playerIdValue;
    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, 9, 0, 512, out System.Int32 prefabIndexValue)) {
      return false;
    }
    msg.prefabIndex = prefabIndexValue;
    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, TrackedBodyState.RelPositionBits, TrackedBodyState.RelPositionMin, TrackedBodyState.RelPositionMax, out Unity.Mathematics.float2 positionValue)) {
      return false;
    }
    msg.position = positionValue;
    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, TrackedBodyState.RotationBits, TrackedBodyState.RotationMin, TrackedBodyState.RotationMax, out System.Single rotationValue)) {
      return false;
    }
    msg.rotation = rotationValue;
    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, TrackedBodyState.VelocityBits, TrackedBodyState.VelocityMin, TrackedBodyState.VelocityMax, out Unity.Mathematics.float2 linearVelocityValue)) {
      return false;
    }
    msg.linearVelocity = linearVelocityValue;
    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, TrackedBodyState.AngularVelocityBits, TrackedBodyState.AngularVelocityMin, TrackedBodyState.AngularVelocityMax, out System.Single angularVelocityValue)) {
      return false;
    }
    msg.angularVelocity = angularVelocityValue;
    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, 32, -256, 2147483648, out System.Int32 expiresOnTickValue)) {
      return false;
    }
    msg.expiresOnTick = expiresOnTickValue;
    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, 1, 0, 2, out int isDeathPrefabValue)) {
      return false;
    }
    msg.isDeathPrefab = isDeathPrefabValue != 0;
    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, 5, 0, 32, out System.Int32 colorIndexValue)) {
      return false;
    }
    msg.colorIndex = colorIndexValue;

    return true;
  }
  public static bool Deserialize(NativeArray<byte> buffer,
                                 ref int bitOffset,
                                 Allocator allocator,
                                 int bufferMask,
                                 out PlayerInput msg) {
    msg = default;

    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, 6, -1, 62, out System.Int32 playerIdValue)) {
      return false;
    }
    msg.playerId = playerIdValue;
    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, 32, -256, 2147483648, out System.Int32 serverTickValue)) {
      return false;
    }
    msg.serverTick = serverTickValue;
    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, -1, 0, 2, out int __commandInt)) {
      return false;
    }

    msg.command = (InputCommand)__commandInt;
    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, QuantizationConstants.InputPayloadBits, QuantizationConstants.InputPayloadMin, QuantizationConstants.InputPayloadMax, out System.Single payloadValue)) {
      return false;
    }
    msg.payload = payloadValue;

    return true;
  }
  public static bool Deserialize(NativeArray<byte> buffer,
                                 ref int bitOffset,
                                 Allocator allocator,
                                 int bufferMask,
                                 out TrackedBodyState msg) {
    msg = default;

    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, 20, 0, 1048576, out System.Int32 idValue)) {
      return false;
    }
    msg.id = idValue;
    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, TrackedBodyState.RelPositionBits, TrackedBodyState.RelPositionMin, TrackedBodyState.RelPositionMax, out Unity.Mathematics.float2 positionValue)) {
      return false;
    }
    msg.position = positionValue;
    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, TrackedBodyState.VelocityBits, TrackedBodyState.VelocityMin, TrackedBodyState.VelocityMax, out Unity.Mathematics.float2 velocityValue)) {
      return false;
    }
    msg.velocity = velocityValue;
    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, TrackedBodyState.AngularVelocityBits, TrackedBodyState.AngularVelocityMin, TrackedBodyState.AngularVelocityMax, out System.Single angularVelocityValue)) {
      return false;
    }
    msg.angularVelocity = angularVelocityValue;
    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, TrackedBodyState.RotationBits, TrackedBodyState.RotationMin, TrackedBodyState.RotationMax, out System.Single rotationValue)) {
      return false;
    }
    msg.rotation = rotationValue;

    return true;
  }
  public static bool Deserialize(NativeArray<byte> buffer,
                                 ref int bitOffset,
                                 Allocator allocator,
                                 int bufferMask,
                                 out PlayerData msg) {
    msg = default;

    if (!Quantization.Dequantize(buffer, ref bitOffset, bufferMask, 6, -1, 62, out System.Int32 idValue)) {
      return false;
    }
    msg.id = idValue;

    return true;
  }
}
