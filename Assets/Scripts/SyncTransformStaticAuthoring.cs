﻿using Unity.Entities;
using UnityEngine;

public class SyncTransformStaticAuthoring : MonoBehaviour, IConvertGameObjectToEntity {
  public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem) {
    dstManager.AddComponentData(entity, new SyncTransform());
    dstManager.AddComponentObject(entity, gameObject);
  }
}
