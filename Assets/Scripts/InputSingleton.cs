﻿using System;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Profiling;
using Unity.Transforms;
using UnityEngine;

public class InputSingleton : MonoBehaviour {
    private GameSystem _game;
    private GameInputSystem _input;
    public static InputSingleton instance;
    public Entity playerEntity;

    private void Start() {
        instance = this;
        _game = World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<GameSystem>();
        _input = World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<GameInputSystem>();
    }

    static readonly ProfilerMarker EachInputMarker = new ProfilerMarker("InputSingleton.Update");
    private void Update() {
        if (playerEntity == Entity.Null) {
            return;
        }
        
        var inputTick = _game.GetJitterBufferedServerTick();

        bool shoot = Input.GetMouseButton(0);
        if (shoot) {
            float3 mousePosition = Input.mousePosition;
            mousePosition.z = Camera.main.nearClipPlane;
            float3 aimLocation = Camera.main.ScreenToWorldPoint(mousePosition);
            aimLocation = aimLocation - World.DefaultGameObjectInjectionWorld.EntityManager
                                             .GetComponentData<Translation>(playerEntity).Value;   
            var shootInput = new PlayerInput {
                command = InputCommand.shoot,
                payload = math.atan2(aimLocation.y, aimLocation.x),
                playerId = _input.playerId,
                serverTick = inputTick,
            };
            _input.QueueInput(shootInput);
        } else {
            var shootInput = new PlayerInput {
                command = InputCommand.shootStop,
                payload = 0,
                playerId = _input.playerId,
                serverTick = inputTick,
            };
            _input.QueueInput(shootInput);
        }
    }
}