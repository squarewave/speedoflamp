﻿using Unity.Entities;
using UnityEngine;

[UpdateInGroup(typeof(PostPhysicsGroup))]
public class EntityExpirySystem : SystemBase {
  protected override void OnUpdate() {
    var serverTick = ServerTickStatic.serverTick;
    Entities.WithStructuralChanges().ForEach((Entity entity, in ExpiresOnTick exp) => {
      if (serverTick >= exp.tick) {
        Object.Destroy(EntityManager.GetComponentObject<GameObject>(entity));
        EntityManager.DestroyEntity(entity);
      }
    }).Run();
  }
}

public struct ExpiresOnTick : IComponentData {
  public int tick;
}
