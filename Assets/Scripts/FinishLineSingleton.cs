﻿using System;
using UnityEngine;

public class FinishLineSingleton : MonoBehaviour {
  public static FinishLineSingleton instance;

  private void Start() {
    instance = this;
  }

  public Bounds GetBounds() {
    return GetComponent<Renderer>().bounds;
  }
}
